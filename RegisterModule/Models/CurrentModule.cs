﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class CurrentModule
    {
        private static CurrentModule instance;
        public static CurrentModule GetInstance()
        {
            if (instance == null) return new CurrentModule();
            else return instance;
        }

        public CurrentModule()
        {
            instance = this;
        }

        public int ModuleID { get; set; }
    }
}