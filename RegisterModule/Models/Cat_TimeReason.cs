﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class Cat_TimeReason
    {
        public int Id { get; set; }
        public string NameReason { get; set; }

        public Cat_TimeReason()
        {
            this.NameReason = string.Empty;
        }
    }
}