﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class FileExtension
    {
        public string MaxFileSize { get; set; }
        public string AllowedAttachmentFileExtensions { get; set; }
        public string Identifier { get; set; }

        public FileExtension()
        {
            this.MaxFileSize = string.Empty;
            this.AllowedAttachmentFileExtensions = string.Empty;
            this.Identifier = string.Empty;
        }
    }
}