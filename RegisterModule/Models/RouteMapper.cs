﻿using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("RegisterModule", "default", "{controller}/{action}", 
                new[] { "Christoc.Modules.RegisterModule.Models" });
        }
    }
}