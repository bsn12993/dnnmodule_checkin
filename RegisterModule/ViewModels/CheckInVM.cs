﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class CheckInVM
    {
        public TimeSpan entryTime { get; set; }
        public string Asignation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string IP { get; set; }

        public bool TimeOk { get; set; }
        public bool LocationOk { get; set; }

        public LocationVM Location { get; set; }
        public TimeVM Time { get; set; }
    }
}