﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class MedicalConsultationVM
    {
        public string Name { get; set; }
        public string Size { get; set; }
        public string Field { get; set; }
    }
}