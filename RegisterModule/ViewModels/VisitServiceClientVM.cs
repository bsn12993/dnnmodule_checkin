﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class VisitServiceClientVM
    {
        public int Id_VisitServiceClient { get; set; }
        public string VisitServiceClient { get; set; }
    }
}