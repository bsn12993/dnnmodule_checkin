﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Util
{
    public class TimeHelper
    {
        public static string ConvertTime(TimeSpan time)
        {
            DateTime d = DateTime.Today.Add(time);
            return d.ToString("hh:mm tt");
        }
    }
}