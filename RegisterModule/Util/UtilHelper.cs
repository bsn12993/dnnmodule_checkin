﻿using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace Christoc.Modules.RegisterModule.Util
{
    public class UtilHelper
    {
        public static string GetIPAddress()
        {
            string ip = string.Empty;
            IPHostEntry ipEntry = Dns.GetHostEntry(GetComputerName());
            IPAddress[] addr = ipEntry.AddressList;
            ip = addr[3].ToString();
            return ip;
        }

        static string GetComputerName()
        {
            return Dns.GetHostName();
        }

        public static Response GetDataEvenLog(int iduser)
        {
            Response response = new Response();
            string ip = "";
            var data = new DotNetNuke.Services.Log.EventLog.EventLogController();

            int records1 = 1;
            int pageSize = 0;
            int pageIndex = 0;

            var dnnLogs = data.GetLogs(0, DotNetNuke.Services.Log.EventLog.EventLogController.EventLogType.LOGIN_SUPERUSER.ToString(), pageSize, pageIndex, ref records1);
            var size = dnnLogs.Count;
            string str = string.Empty;
            var log = dnnLogs.Where(x => x.LogCreateDate.ToShortDateString() == DateTime.Now.Date.AddDays(-1).ToShortDateString() && x.LogUserID == iduser).FirstOrDefault();

            str = log.LogProperties.Serialize();
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(str);
                XmlNodeList xLogProperties = xml.GetElementsByTagName("LogProperties");
                XmlNodeList xLogProperty = ((XmlElement)xLogProperties[0]).GetElementsByTagName("LogProperty");

                foreach (XmlElement xPropertyValue in xLogProperty)
                {
                    int i = 0;
                    XmlNodeList IP = xPropertyValue.GetElementsByTagName("PropertyValue");
                    ip = IP[i].InnerText;
                    response.IsSuccess = true;
                    //response.Result = ip + " -> " + log.LogEventID;
                    response.Result = ip;
                }
            }
            catch(Exception e)
            {
                response.IsSuccess = false;
                response.Message = e.Message;
            }
            return response;
        }
        /// <summary>
        /// Metodo que valida si se ha seleccionado alguna opción de razones del cambio de ubicación
        /// </summary>
        /// <param name="model">objeto con los opciones</param>
        /// <returns>retorna true|false indicando que se encontro una opcion seleccionada</returns>
        public static bool ValidateLocationOption(LocationVM model)
        {
            if (model.NewAsignationOk) return true;
            else if (model.VisitServiceClientOk) return true;
            else if (model.HomeOfficeOk) return true;
            else if (model.BackOfficeOk) return true;
            else if (model.OtherReasonOk)return true;
            else return false;
        }
        /// <summary>
        /// Metodo que valida si se ha seleccionado alguna opción de razones del la hora de entrada
        /// </summary>
        /// <param name="model">objeto con los opciones</param>
        /// <returns>retorna true|false indicando que se encontro una opcion seleccionada</returns>
        public static bool ValidateTimeOption(TimeVM model)
        {
            if (model.VisitServiceClientOk) return true;
            else if (model.MedicalConsultationOk) return true;
            else if (model.PersonalIncidentOk) return true;
            else if (model.CompensationHoursOk) return true;
            else if (model.OtherReasonOk) return true;
            else return false;
        }
        /// <summary>
        /// Metodo que valida el campo en base a la opción seleccionada de razones de la hora de entrada
        /// </summary>
        /// <param name="model">objeto con los opciones</param>
        /// <returns>retorna true|false</returns>
        public static object[] ValidateFieldTime(TimeVM model)
        {
            if (model.VisitServiceClientOk)
            {
                if (string.IsNullOrEmpty(model.VisitServiceClientTime))
                {
                    return new object[2]
                    {
                        false,
                        "Se debe ingresar el cliente donde se realiza la visita o servicio"
                    };
                }
            }
            else if (model.MedicalConsultationOk)
            {
                if (string.IsNullOrEmpty(model.MedicalConsultation))
                {
                    return new object[2]
                    {
                        false,
                        "Se debe cargar el comprobante medico"
                    };
                }
            }
            else if (model.PersonalIncidentOk)
            {
                if (string.IsNullOrEmpty(model.PersonalIncident))
                {
                    return new object[2]
                    {
                        false,
                        "Se debe describir el incidente personal"
                    };
                }
            }
            else if (model.CompensationHoursOk)
            {
                if (string.IsNullOrEmpty(model.CompensationHours))
                {
                    return new object[2]
                    {
                        false,
                        "Se debe ingresar el nombre del supervisor/jefe directo que lo autorizo"
                    };
                }
            }
            else if (model.OtherReasonOk)
            {
                if (string.IsNullOrEmpty(model.OtherReason))
                {
                    return new object[2]
                    {
                        false,
                        "Se debe describir la razón del registro fuera del limite"
                    };
                }
            }
            return new object[2]
                    {
                        true,
                        ""
                    };
        }
        /// <summary>
        /// Metodo que valida el campo en base a la opcion seleccionada de razones del cambio de ubicación
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static object[] ValidateFieldLocation(LocationVM model)
        {
            if (model.NewAsignationOk)
            {
                if (string.IsNullOrEmpty(model.NewAsignation))
                {
                    return new object[2]
                   {
                        false,
                        "Se debe ingresar el cliente donde se realiza la visita o servicio para la nueva asignación"
                   };
                }
            }
            if (model.VisitServiceClientOk)
            {
                if (string.IsNullOrEmpty(model.VisitServiceClientLocation))
                {
                    return new object[2]
                   {
                        false,
                        "Se debe ingresar el cliente donde se realiza la visita o servicio"
                   };
                }
            }
            if (model.HomeOfficeOk)
            {
                if (string.IsNullOrEmpty(model.HomeOffice))
                {
                    return new object[2]
                   {
                        false,
                        "Se debe ingresar el nombre del supervisor/jefe directo que lo autorizo"
                   };
                }
            }
            if (model.OtherReasonOk)
            {
                if (string.IsNullOrEmpty(model.OtherReason))
                {
                    return new object[2]
                   {
                        false,
                        "Se debe describir la razón del registro fuera desde una ubicación diferente"
                   };
                }
            }
            return new object[2]
                   {
                        true,
                        ""
                   };
        }
        /// <summary>
        /// Metodo que recupera el texto de la opción seleccionada de ubicación
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public static string GetReasonLocation(LocationVM location)
        {
            string msg = string.Empty;
            if (location.BackOfficeOk) msg = string.Format("{0} {1}", "Regreso a Oficina TASISOFT", location.BackOffice);
            else if (location.HomeOfficeOk) msg = string.Format("{0} {1}", "Trabajo desde casa autorizado por", location.HomeOffice);
            else if (location.NewAsignationOk) msg = string.Format("{0} {1}", "Nueva asignación con el Cliente", location.NewAsignation);
            else if (location.OtherReasonOk) msg = string.Format("{0} {1}", "Otro motivo: ", location.OtherReason);
            else if (location.VisitServiceClientOk) msg = string.Format("{0} {1}", "Visita o Servicio con el Cliente ", location.VisitServiceClientLocation);
            return msg;
        }
        /// <summary>
        /// Metodo que recupera el texto de la opción seleccionada de hora
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetReasonTime(TimeVM time)
        {
            string msg = string.Empty;
            if (time.CompensationHoursOk) msg = string.Format("{0} {1}", "Permuta o Compensación de Horas autorizado por ", time.CompensationHours);
            else if (time.MedicalConsultationOk) msg = string.Format("{0} {1}", "Archivo adjunto del comprobante medico ", time.MedicalConsultationFileName);
            else if (time.OtherReasonOk) msg = string.Format("{0} {1}", "Otro Motivo: ", time.OtherReason);
            else if (time.PersonalIncidentOk) msg = string.Format("{0} {1}", "Incidente Personal: ", time.PersonalIncident);
            else if (time.VisitServiceClientOk) msg = string.Format("{0} {1}", "Visita o Servicio con el Cliente ", time.VisitServiceClientTime);
            return msg;
        }
        /// <summary>
        /// Metodo que guarda el archivo seleccionado en una ubicación fisica
        /// </summary>
        /// <param name="base64">archivo en base</param>
        /// <param name="idmodule">id del modulo actual</param>
        /// <param name="nameFile">nombre del archivo original</param>
        public static string SaveFileTemp(string base64, int idmodule,string nameFile)
        {
            var file = base64.Split(',')[1];
            byte[] imageBytes = Convert.FromBase64String(file);
            var root = HttpContext.Current.Server.MapPath("~");
            var getFolder = CreateFolder(Path.Combine(root, "Portals/0/LiveHelpdesk"), idmodule);
            var validateFile = ValidateFile(getFolder, nameFile);
            var path = Path.Combine(getFolder, validateFile);
            File.WriteAllBytes(path, imageBytes);
            return validateFile;
        }

        public static void DeleteFile(string base64, int idmodule, List<MedicalConsultationVM> nameFile)
        {
            var file = base64.Split(',')[1];
            byte[] imageBytes = Convert.FromBase64String(file);
            var root = HttpContext.Current.Server.MapPath("~");
            var getFolder = CreateFolder(Path.Combine(root, "Portals/0/LiveHelpdesk"), idmodule);
            foreach(var f in nameFile)
            {
                var fileDel = Path.Combine(getFolder, f.Name);
                if (File.Exists(fileDel))
                {
                    File.Delete(fileDel);
                }
            }
        }
        /// <summary>
        /// Metodo que valida la extención del archivo seleccionado
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool ValidateExtension(string fileExtension, string file)
        {
            var lstFileExtensions = fileExtension.Split(',');
            if (lstFileExtensions.Contains(file.Split('/')[1])) return true;
            else return false;
        }
        /// <summary>
        /// Metodo que valida si el archivo ya existe con el mismo nombre, en caso de existir se le concatena la hora en milisegundos
        /// </summary>
        /// <param name="path">ruta fisica</param>
        /// <param name="nameFile">nombre del archivo</param>
        /// <returns>retorna el nombre del archivo</returns>
        public static string ValidateFile(string path, string nameFile)
        {
            var file = Path.Combine(path, nameFile);
            if (File.Exists(file))
            {
                var name = nameFile.Split('.');
                var nFile = name[0] + DateTime.Now.Millisecond;
                return string.Format("{0}{1}{2}", nFile, ".", name[1]);
            }
            return nameFile;
        }
        /// <summary>
        /// Metodo que valida si el directorio existe.
        /// en caso de no existir se crea
        /// en caso de exister se retorna la ruta
        /// </summary>
        /// <param name="path">ruta a validar</param>
        /// <param name="idmodule">id del modulo actual</param>
        /// <returns>retorna la ruta de ubicacion</returns>
        public static string CreateFolder(string path,int idmodule)
        {
            var folder = Path.Combine(path, idmodule.ToString());
            if (Directory.Exists(folder))
            {
                return folder;
            }
            Directory.CreateDirectory(folder);
            return folder;
        }

        /// <summary>
        /// Metodo que recupera el id del modulo de HelpDesk (Tickets) para poder obtener o crear la carpeta
        /// en donde se guardaran los archivos.
        /// </summary>
        /// <returns></returns>
        public static int GetModuleID()
        {
            int moduleID = 0;
            var ModuleInfo = DotNetNuke.Entities.Modules.ModuleController.Instance.GetAllModules();
            foreach (var mod in ModuleInfo)
            {
                var moduleInfo = (DotNetNuke.Entities.Modules.ModuleInfo)mod;
                if (moduleInfo.ModuleTitle.Equals("Tickets"))
                {
                    moduleID = moduleInfo.ModuleID;
                    break;
                }
            }
            return moduleID;
        }

        /// <summary>
        /// Metodo que lee un archivos recuperando el contenido en una cadena de caracteres
        /// </summary>
        /// <param name="rutaArchivo"></param>
        /// <returns></returns>
        public static string ReadFile(string rutaArchivo)
        {
            string contenido = string.Empty;
            using (StreamReader sr = new StreamReader(rutaArchivo))
            {
                try
                {
                    contenido = sr.ReadToEnd();
                    sr.Close();
                }
                catch(Exception e)
                {
                    return default(string);
                }
            }
            return contenido;
        }
    }
}