﻿using CheckInTasiSoft.DAL;
using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Util;
using Christoc.Modules.RegisterModule.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Services
{
    public class CheckInService
    {
        DataAccess DataAccess = new DataAccess();

        #region Register Entry/Departure
        /// <summary>
        /// Metodo para el registro de hora de entrada
        /// </summary>
        /// <param name="register">objeto con los datos para el registro</param>
        /// <returns></returns>
        public Response SetEntryTime(Register register, Ticket ticket, CheckInVM checkIn)
        {
            Response response = new Response();
            int getAgentID = 0;
            //se valida si cumplio con la ubicación 
            if (!checkIn.LocationOk)
            {
                var subject = (Cat_LocationReason)DataAccess.GetLocationReasonById(checkIn.Location.IdLocationReason).Result;
                ticket.Subject = string.Format("{0} {1}", "Registro desde otra ubicación diferente", subject.NameReason);
                ticket.Message = string.Format("{0}{1}{2}", "<p>Registro desde otra ubicación diferente -", Util.UtilHelper.GetReasonLocation(checkIn.Location), "</p>");
                //Recupera el id del usuario asignado a los tickets, normalmente es para Carlos_Suarez con el id 6
                getAgentID = DataAccess.GetUserIDAssignation("Carlos_Suarez");
            }
            //se valida si se cumplio con el horario de entrada para almacenar los datos para el generar el ticket
            if (!checkIn.TimeOk)
            {
                if (checkIn.Time.MedicalConsultationOk)
                {
                    List<MedicalConsultationVM> attachments = new List<MedicalConsultationVM>();
                    attachments.Add(new MedicalConsultationVM
                    {
                        Name = checkIn.Time.MedicalConsultationFileName,
                        Size = checkIn.Time.MedicalConsultationFileSize
                    });
                    var json = JsonConvert.SerializeObject(attachments);
                    ticket.Attachments = json;
                }
                var subject = (Cat_TimeReason)DataAccess.GetTimeReasonById(checkIn.Time.IdTimeReason).Result;

                ticket.Message += (ticket.Message.Length == 0) ? "</br>" : "";
                ticket.Message += string.Format("{0}{1}{2}", "<p>Registro fuera del horario limite establecido ", Util.UtilHelper.GetReasonTime(checkIn.Time), "</p>");
                ticket.Subject += string.Format(" {0} {1}", "Registro fuera del horario limite establecido -", subject.NameReason);
                if (getAgentID == 0)
                {
                    //Si aun no se ha recuperado el id del usuario asignado para el ticket se recupera 
                    //Recupera el id del usuario asignado a los tickets, normalmente es para Carlos_Suarez con el id 6
                    getAgentID = DataAccess.GetUserIDAssignation("Carlos_Suarez");
                }
            }

            if (!checkIn.LocationOk || !checkIn.TimeOk)
                return DataAccess.InsertEntryTime(register, ticket, getAgentID); 
            else
                return DataAccess.InsertEntryTime(register, null, getAgentID);
        }
        /// <summary>
        ///  Metodo para el registro de hora de salida
        /// </summary>
        /// <param name="register">objeto con los datos para el registro</param>
        /// <returns></returns>
        public Response SetDepartureTime(Register register)
        {
            return DataAccess.InsertDepartureTime(register);
        }
        /// <summary>
        /// Metodo para recuperar registros del usuario en sesión en base a su id
        /// </summary>
        /// <param name="id">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUserById(int id)
        {
            return DataAccess.GetUserById(id);
        }
        /// <summary>
        /// Metodo para recuperar y validar los registros de dia presente del usuario
        /// </summary>
        /// <param name="id">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUSerRecordsToday(int id)
        {
            return DataAccess.GetUSerRecordsToday(id);
        }

        public Response ValidateBirthdayUser(string datebirthday, int iduser)
        {
            var dBirthday = datebirthday.ConvertStringToDateTime();
            return DataAccess.ValidateBirthdayUser(dBirthday, iduser);
        }
        #endregion

        #region Catalogs
        /// <summary>
        /// Metodo que recupera los datos del catalogo de motivos-ubicación
        /// </summary>
        /// <returns></returns>
        public Response GetLocationReason()
        {
            return DataAccess.GetLocationReason();
        }
        /// <summary>
        /// Metodo que recupera los datos del catalogo de motivos-hora
        /// </summary>
        /// <returns></returns>
        public Response GetTimeReason()
        {
            return DataAccess.GetTimeReason();
        }
        #endregion

        #region Tickets
        /// <summary>
        /// Metod que genera el ticken insertando en la tabla de help desk
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="checkIn"></param>
        /// <returns></returns>
        //public Response GenerateTicket(Ticket ticket, CheckInVM checkIn)
        //{
        //    List<Ticket> lstTickets = new List<Ticket>();
        //    Response response = new Response();
        //    if (!checkIn.LocationOk)
        //    {
        //        var subject = (Cat_LocationReason)DataAccess.GetLocationReasonById(checkIn.Location.IdLocationReason).Result;
        //        lstTickets.Add(new Ticket
        //        {
        //            ModuleID = ticket.ModuleID,
        //            ChannelID = ticket.ChannelID,
        //            ReporterID = ticket.ReporterID,
        //            Email = ticket.Email,
        //            Name = ticket.Name,
        //            Phone = ticket.Phone,
        //            Subject = subject.NameReason,
        //            Message = string.Format("{0}{1}{2}", "<p>", Util.UtilHelper.GetReasonLocation(checkIn.Location), "</p>"),
        //            StatusID = ticket.StatusID,
        //            CreatedOn = ticket.CreatedOn
        //        });
        //    }
        //    if (!checkIn.TimeOk)
        //    {
        //        List<MedicalConsultationVM> attachments = new List<MedicalConsultationVM>();
        //        attachments.Add(new MedicalConsultationVM
        //        {
        //            Name = checkIn.Time.MedicalConsultationFileName,
        //            Size = checkIn.Time.MedicalConsultationFileSize
        //        });
        //        var json = JsonConvert.SerializeObject(attachments);
        //        var subject = (Cat_TimeReason)DataAccess.GetTimeReasonById(checkIn.Time.IdTimeReason).Result;
        //        lstTickets.Add(new Ticket
        //        {
        //            ModuleID = ticket.ModuleID,
        //            ChannelID = ticket.ChannelID,
        //            ReporterID = ticket.ReporterID,
        //            Email = ticket.Email,
        //            Name = ticket.Name,
        //            Phone = ticket.Phone,
        //            Subject = subject.NameReason,
        //            Attachments = json,
        //            Message = string.Format("{0}{1}{2}", "<p>", Util.UtilHelper.GetReasonTime(checkIn.Time), "</p>"),
        //            StatusID = ticket.StatusID,
        //            CreatedOn = ticket.CreatedOn
        //        });
        //    }
        //    return DataAccess.GenerateTicket(lstTickets); 
        //}
        /// <summary>
        /// Metodo que recupera las extenciones permitidas
        /// </summary>
        /// <returns></returns>
        public Response GetFileExtensions()
        {
            return DataAccess.GetFileExtensions();
        }
        #endregion
    }
}