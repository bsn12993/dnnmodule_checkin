﻿using CheckInTasiSoft.DAL;
using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Services;
using Christoc.Modules.RegisterModule.Util;
using Christoc.Modules.RegisterModule.ViewModels;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.UI.Modules;
using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Christoc.Modules.RegisterModule.Controllers
{
    public class CheckInController : DnnController
    {
        CheckInService checkInService = new CheckInService();

        // GET: CheckIn
        public ActionResult Index()
        {
            CurrentModule.GetInstance().ModuleID = 
                (CurrentModule.GetInstance().ModuleID == 0) ? 
                UtilHelper.GetModuleID() : 
                CurrentModule.GetInstance().ModuleID;
            var module = CurrentModule.GetInstance().ModuleID;
            List<Cat_LocationReason> locationReasons = new List<Cat_LocationReason>();
            List<Cat_TimeReason> timeReasons = new List<Cat_TimeReason>();

            var userData = UserController.Instance.GetCurrentUserInfo();
            int validateEntry = 0;
            int validateDeparture = 0;
            var user = new UserVM()
            {
                User_Id = userData.UserID,
                Name = userData.FirstName,
                LastName = userData.LastName,
                Area = userData.Email,
                EntryTime = "",
                DepartureTime = ""
            };

            var data = checkInService.GetUSerRecordsToday(userData.UserID);
            if (data != null)
            {
                var noEntry = new TimeSpan(0, 0, 0);
                validateEntry = data.Entry.CompareTo(noEntry);
                validateDeparture = data.Departure.CompareTo(noEntry);
                user.EntryTime = (validateEntry == 0) ? "Entrada no registrada" : TimeHelper.ConvertTime(data.Entry);
                user.DepartureTime = (validateDeparture == 0) ? "Salida no registrada" : TimeHelper.ConvertTime(data.Departure);
            }
            else
            {
                user.EntryTime = "Entrada no registrada ";
                user.DepartureTime = "Salida no registrada";
                validateEntry = 0;
                validateDeparture = 0;
            }

            var reasonLocations = checkInService.GetLocationReason();
            if (reasonLocations.IsSuccess) locationReasons = (List<Cat_LocationReason>)reasonLocations.Result;

            var reasonTimes = checkInService.GetTimeReason();
            if (reasonTimes.IsSuccess) timeReasons = (List<Cat_TimeReason>)reasonLocations.Result;

            ViewBag.EntryTime = user.EntryTime;
            ViewBag.DepartureTime = user.DepartureTime;
            ViewBag.IsDisabledDeparture = validateDeparture;
            ViewBag.IsDisabledEntry = validateEntry;
            ViewBag.LocationCatalog = locationReasons;
            ViewBag.TimeCatalog = timeReasons;

            return View();
        }
    }
}