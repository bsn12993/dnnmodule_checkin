﻿
//URL de la web api para realizar la peticiones
var URL = "/tasinet2/DesktopModules/RegisterModule/API/ModuleTask/"; //URL local
var base64 = "";

//al cargar la pagina se ocultan los div's que contienen los campos necesarios en caso de que se requiera
//los div's corresponden a motivos de cambio de ubicación
$("#div_loading").hide();
$("#div_MotivoUbicacion").hide();   
$("#NewAssignationLocation").hide();
$("#VisitServiceClientLocation").hide();
$("#HomeOfficeLocation").hide();
$("#OtherReasonLocation").hide();

//al cargar la pagina se ocultan los div's que contienen los campos necesarios en caso de que se requiera
//los div's corresponden a motivos de registro fuera de la hora limite
$("#div_MotivoHoraEntrada").hide();   
$("#VisitServiceClientTime").hide();
$("#MedicalConsultation").hide();
$("#PersonalIncident").hide();
$("#CompensationHours").hide();
$("#OtherReason").hide();

//al cargar la pagina se ocultan los div's que seran usados para mostrar los mensajes de validaciones cuando se seleccione alguna opcion
//de la lista de motivos de cambio de ubicación y hora fuera del limite
//este div corresponde al combo de cambio de ubicación
$("#div_alert_opcion_locacion").hide();
//este div corresponde al combo de razones por la que se registro fuera de la hora limite
$("#div_alert_opcion_time").hide();

//al cargar la pagina se ocultan los div's que seran usados para mostrar los mensajes de validaciones de los campos cuando se seleccione un motivo de 
//la lista de opciones
//este sera usado para la hora entrada
$("#div_mensajeValidacionHoraEntrada").hide();
$("#mensajeValidacionHoraEntrada").html("");

//este sera usado para el cambio de ubicación
$("#div_mensajeValidacionUbicacion").hide();
$("#mensajeValidacionUbicacion").html("");

///al cargar la pagina se ocultan el div que sera usado para mostrar el mensaje de exito cuando se registre la hora entrada/salida
$("#div_alertsuccess").hide();
$("#txt_mensajeEntradaRegistro").html("");


GetBirthdayUser(URL + "GetBirthdayUser");
GetDateNew();
//GetPublicIP();                  //se invoca el metodo para recuperar la ip publica al cargar la pagina/modulo
GetLocation();                  //se invoca el metodo para recuperar la latitud y longitud de la ubicación fisica al cargar la pagina/modulo

var entryTime = "";             //se crea e inicializa varible bandera para almacenar la hora de entrada
var departureTime = "";         //se crea e inicializa varible bandera para almacenar la hora de salida

var objDate = {
    Date: '',
    IsSuccess: false,
    Message: ''
}

//objeto para almacenar los valores de las opciones cuando se registre fuera de la hora limite
var objTime = {
    VisitServiceClientOk: false,    //establece con true|false si fue seleccionada la opcion "visita/servicio a cliente"
    VisitServiceClientTime: '',     //almacena el valor de la opción la opcion "visita/servicio a cliente"
    MedicalConsultationOk: false,   //establece si fue seleccionada la opcion "consulta medica"
    MedicalConsultation: "",        //almacena el valor de la opción "consulta medica"
    MedicalConsultationFileExtension: '',//almacena la extencion del archivo
    MedicalConsultationFileName: '',
    MedicalConsultationFileSize: '',
    PersonalIncidentOk: false,      //establece con true|false si fue seleccionada la opción "incidente personal"
    PersonalIncident: '',           //almacena el valor de la opción "incidente personal"
    CompensationHoursOk: false,     //establece con true|false si fue seleccionada la opción "compensación de horas"
    CompensationHours: '',          //almacena el valor de la opción "compensación de horas"
    OtherReasonOk: false,           //establece con true|false si fue seleccionada la opción "otro motivo"
    OtherReason: '',                //almacena el valor de la opción "otro motivo"
    IdTimeReason: ''                //almacena el id de la opción del motivo seleccionada
};

//objeto para almacenar los valores de las opciones cuando se registre desde otra ubicacaón
var objLocation = {
    NewAsignationOk: false,         //establece con true|false si fue seleccionada la opcion "nueva asignación"
    NewAsignation: '',              //almacena el valor de la opción la opcion "nueva asignación"
    VisitServiceClientOk: false,    //establece con true|false si fue seleccionada la opcion "visita/servicio a cliente"
    VisitServiceClientLocation: '', //almacena el valor de la opción la opcion "visita/servicio a cliente"
    HomeOfficeOk: false,            //establece con true|false si fue seleccionada la opcion "trabajo desde casa"
    HomeOffice: '',                 //almacena el valor de la opción la opcion "trabajo desde casa"
    BackOfficeOk: false,            //establece con true|false si fue seleccionada la opcion "regreso a oficina"
    BackOffice: '',                 //almacena el valor de la opción la opcion "regreso a oficina"
    OtherReasonOk: false,           //establece con true|false si fue seleccionada la opcion "otro motivo"
    OtherReason: '',                //almacena el valor de la opción la opcion "otro motivo"
    IdLocationReason: ''            //almacena el id de la opción del motivo seleccionada
};


//objeto principal que almacenara los datos para enviarlos a la petición
var obj = {
    EntryTime: '',              //almacena la hora de entrada
    Asignation: 0,              //almacena la asignación
    Latitude: '',               //almacena la latitud de la ubicación donde se registro
    Longitude: '',              //almacena la longitud de la ubicación donde se registro
    IP: '',                     //almacena la dirección ip de la ubicación donde se registro
    TimeOk: true,               //establece un valor true|false.en caso de registrar dentro de la hora limite sera true, en caso constrario sera false
    Time: objTime,              //almacena el objeto objTime
    LocationOk: true,           //establece un valor true|false.en caso de registrar desde la misma ubicación sera true, en caso constrario sera false
    Location: objLocation,      //almacena el objeto objLocation
    TimeReason: false,
    LocationReason: false,
    entryOk: true
};                            

// se valida si la pagina se esta visualizando desde un dispositivo mobile o pc para adaptar elementos a la pantalla
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    console.log('Esto es un dispositivo móvil');
    $("#btn_entrar").css("width", "100%");
    $("#btn_salir").css("width", "100%");
}
else {
    console.log('Esto es un dispositivo PC');
    $("#btn_entrar").css("width", "80px");
    $("#btn_salir").css("width", "80px");
}

//metodo que establece los icons "info" en los campos al cargar la pagina
setIconDefaultField();

//********************************************************************************************//
//*********************************** Start Events Methods ***********************************//
//********************************************************************************************//

//Evento se ejecuta al dar click en boton entrar
$("#btn_entrar").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).attr('disabled', 'disabled');
    $("#div_alertsuccess").hide();
    $("#txt_mensajeEntradaRegistro").html("");
    var url = "";
    var registroEntrada = new Date();
    var hora = registroEntrada.getHours();
    var minuto = registroEntrada.getMinutes();
    var segundo = registroEntrada.getSeconds();
    entryTime = hora + ":" + minuto + ":" + segundo;

    GetDateNew();
    //GetPublicIP();
    GetLocation();

    if (!objDate.IsSuccess) {
        alert(objDate.Message);
        return;
    }

    //Se valida la recuperación de la ubicación
    var location = $("#txt_location").html();
    if (location == "User denied the request for Geolocation.") {
        alert("No se pudo recuperar la ubicación: " + location);
        $("#btn_entrar").removeAttr('disabled');
        return;
    }
    else if (location == "Location information is unavailable.") {
        alert("No se pudo recuperar la ubicación: " + location);
        $("#btn_entrar").removeAttr('disabled');
        return;
    }
    else if (location == "The request to get user location timed out.") {
        alert("No se pudo recuperar la ubicación: " + location);
        $("#btn_entrar").removeAttr('disabled');
        return;
    }
    else if (location == "An unknown error occurred.") {
        alert("No se pudo recuperar la ubicación: " + location);
        $("#btn_entrar").removeAttr('disabled');
        return;
    }
    else if (location == "") {
        alert("No se pudo recuperar la ubicación: ");
        $("#btn_entrar").removeAttr('disabled');
        return;
    }

    if (!obj.TimeOk) {
        if (obj.Time.VisitServiceClientOk) {
            obj.Time.VisitServiceClientTime = $("#txt_clienteTime").val();
            if (obj.Time.VisitServiceClientTime == "") {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("Se debe ingresar el cliente donde se realiza la visita o servicio");
                return;
            }
        }
        else if (obj.Time.MedicalConsultationOk) {
            var file = document.getElementById("file");
            if (file.files.length == 0) {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("Se debe cargar el comprobante medico");
                return;
            }
            obj.Time.MedicalConsultation = $("#contenido-archivo").val();
            if (obj.Time.MedicalConsultation == "") {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("No se ha cargado el archivo");
                return;
            }
        }
        else if (obj.Time.PersonalIncidentOk) {
            obj.Time.PersonalIncident = $("#txt_incidentePersonal").val();
            if (obj.Time.PersonalIncident == "") {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("Se debe describir el incidente personal");
                return;
            }
        }
        else if (obj.Time.CompensationHoursOk) {
            obj.Time.CompensationHours = $("#CompensacionHoras").val();
            if (obj.Time.CompensationHours == "") {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("Se debe ingresar el nombre del supervisor/jefe directo que lo autorizo");
                return;
            }
        }
        else if (obj.Time.OtherReasonOk) {
            obj.Time.OtherReason = $("#txt_otroMotivoTime").val();
            if (obj.Time.OtherReason == "") {
                $("#div_mensajeValidacionHoraEntrada").show();
                $("#mensajeValidacionHoraEntrada").html("Se debe describir la razón del registro fuera del limite");
                return;
            }
        }
        else {
            $("#div_alert_opcion_time").show();
            $("#txt_validacionOpcionTime").html("Se debe seleccionar una opcion del listado");
            $("#btn_entrar").removeAttr('disabled');
            return;
        }
    }

    if (!obj.LocationOk) {
        if (obj.Location.NewAsignationOk) {
            obj.Location.NewAsignation = $("#txt_nuevaAsignacion").val();
            if (obj.Location.NewAsignation == "") {
                $("#div_mensajeValidacionUbicacion").show();
                $("#mensajeValidacionUbicacion").html("Se debe ingresar el cliente donde se realiza la visita o servicio para la nueva asignación");
                return;
            }
        }
        else if (obj.Location.VisitServiceClientOk) {
            obj.Location.VisitServiceClientLocation = $("#txt_clienteLocation").val();
            if (obj.Location.VisitServiceClientLocation == "") {
                $("#div_mensajeValidacionUbicacion").show();
                $("#mensajeValidacionUbicacion").html("Se debe ingresar el cliente donde se realiza la visita o servicio");
                return;
            }
        }
        else if (obj.Location.HomeOfficeOk) {
            obj.Location.HomeOffice = $("#HomeOffice").val();
            if (obj.Location.HomeOffice == "") {
                $("#div_mensajeValidacionUbicacion").show();
                $("#mensajeValidacionUbicacion").html("Se debe ingresar el nombre del supervisor/jefe directo que lo autorizo");
                return;
            }
        }
        else if (obj.Location.BackOfficeOk) {
            obj.Location.BackOffice = "";
        }
        else if (obj.Location.OtherReasonOk) {
            obj.Location.OtherReason = $("#txt_otroMotivoLocation").val();
            if (obj.Location.OtherReason == "") {
                $("#div_mensajeValidacionUbicacion").show();
                $("#mensajeValidacionUbicacion").html("Se debe describir la razón del registro fuera desde una ubicación diferente");
                return;
            }
        }
        else {
            $("#div_alert_opcion_locacion").show();
            $("#txt_validacionOpcionLocation").html("Se debe seleccionar una opcion del listado");
            $("#btn_entrar").removeAttr('disabled');
            return;
        }
    }

    url = URL + "EntryTime";
    obj.IP = $("#txt_ip").html();
    obj.Latitude = $("#txt_location").html().split(",")[0];
    obj.Longitude = $("#txt_location").html().split(",")[1];
    if (obj.Latitude != '' && obj.Longitude != '' && obj.IP != '') {
        obj.EntryTime = entryTime;
        obj.Asignation = '';
        SetTimeEntry(url, JSON.stringify(obj));
    }
    //if (obj.IP != '') {
    //    obj.EntryTime = entryTime;
    //    obj.Asignation = '';
    //    SetTimeEntry(url, JSON.stringify(obj));
    //}
    else {
        var msg = "";
        if (obj.Latitude == "") msg += "No se pudo recuperar la ubicación (Latitude)\n";
        if (obj.Longitude == "") msg += "No se pudo recuperar la ubicación (Longitude)\n";
        if (obj.IP == "") msg += "No se pudo recuperar la IP\n";
        $("#btn_entrar").removeAttr('disabled');
        alert(msg);
    }

});

//Evento se ejecuta al dar click en boton salir
$("#btn_salir").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $("#div_alertsuccess").hide();
    $("#txt_mensajeEntradaRegistro").html("");
    var url = URL + "DepartureTime";
    var registroSalida = new Date();
    var hora = registroSalida.getHours();
    var minuto = registroSalida.getMinutes();
    var segundo = registroSalida.getSeconds();
    departureTime = hora + ":" + minuto + ":" + segundo;
    if (!obj.entryOk) {
        $("#div_alertsuccess").show();
        $("#div_alertsuccess").removeClass('alert alert-success').addClass('alert alert-danger');
        $("#txt_mensajeEntradaRegistro").html("No se puede registrar la salida sin haber registrado la entrada primero.");
        return;
    }
    SetTimeDeparture(url, JSON.stringify(departureTime));
});

//Evento donde en base a la opción elegida se muestran los campos necesarios para el registro del motivo cuando se 
//registra fuera del horario limite
$("#cmb_MotivoHorario").change(function () {
    var id = $(this).val();
    var name = $(this).text();
    //alert(id + " - " + name);
    switch (id) {
        case "0":
            $("#VisitServiceClientTime").hide();
            $("#MedicalConsultation").hide();
            $("#PersonalIncident").hide();
            $("#CompensationHours").hide();
            $("#OtherReason").hide();
            setActiveOptionTime('', false);
            clearFieldsTime();
            emptyFieldObjectTime('0');
            setIconDefaultField();
            $("#div_alert_opcion_time").show();
            $("#txt_validacionOpcionTime").html("Se debe seleccionar una opcion del listado");
            break;
        case "1":
            $("#VisitServiceClientTime").show();
            $("#MedicalConsultation").hide();
            $("#PersonalIncident").hide();
            $("#CompensationHours").hide();
            $("#OtherReason").hide();
            setActiveOptionTime('VisitServiceClientOk', true);
            clearFieldsTime();
            emptyFieldObjectTime(id);
            setIconDefaultField();
            $("#div_alert_opcion_time").hide();
            $("#txt_validacionOpcionTime").html("");
            break;
        case "2":
            $("#VisitServiceClientTime").hide();
            $("#MedicalConsultation").show();
            $("#PersonalIncident").hide();
            $("#CompensationHours").hide();
            $("#OtherReason").hide();
            setActiveOptionTime('MedicalConsultationOk', true);
            clearFieldsTime();
            emptyFieldObjectTime(id);
            setIconDefaultField();
            $("#div_alert_opcion_time").hide();
            $("#txt_validacionOpcionTime").html("");
            break;
        case "3":
            $("#VisitServiceClientTime").hide();
            $("#MedicalConsultation").hide();
            $("#PersonalIncident").show();
            $("#CompensationHours").hide();
            $("#OtherReason").hide();
            setActiveOptionTime('PersonalIncidentOk', true);
            clearFieldsTime();
            emptyFieldObjectTime(id);
            setIconDefaultField();
            $("#div_alert_opcion_time").hide();
            $("#txt_validacionOpcionTime").html("");
            break;
        case "4":
            $("#VisitServiceClientTime").hide();
            $("#MedicalConsultation").hide();
            $("#PersonalIncident").hide();
            $("#CompensationHours").show();
            $("#OtherReason").hide();
            setActiveOptionTime('CompensationHoursOk', true);
            clearFieldsTime();
            emptyFieldObjectTime(id);
            setIconDefaultField();
            $("#div_alert_opcion_time").hide();
            $("#txt_validacionOpcionTime").html("");
            break;
        case "5":
            $("#VisitServiceClientTime").hide();
            $("#MedicalConsultation").hide();
            $("#PersonalIncident").hide();
            $("#CompensationHours").hide();
            $("#OtherReason").show();
            setActiveOptionTime('OtherReasonOk', true);
            clearFieldsTime();
            emptyFieldObjectTime(id);
            setIconDefaultField();
            $("#div_alert_opcion_time").hide();
            $("#txt_validacionOpcionTime").html("");
            break;
    }
});

//Evento donde en base a la opción elegida se muestran los campos necesarios para el registro del motivo cuando se 
//registra desde una ubicación diferente
$("#cmb_MotivoUbicacion").change(function () {
    var id = $(this).val();
    switch (id) {
        case "0":
            $("#NewAssignationLocation").hide();
            $("#VisitServiceClientLocation").hide();
            $("#HomeOfficeLocation").hide();
            $("#OtherReasonLocation").hide();
            setTrueOptionLocation('', false);
            clearFieldsLocation();
            emptyFieldObjectLocation('0');
            setIconDefaultField();
            $("#div_alert_opcion_locacion").show();
            $("#txt_validacionOpcionLocation").html("Se debe seleccionar una opcion del listado");
            break;
        case "1":
            $("#NewAssignationLocation").show();
            $("#VisitServiceClientLocation").hide();
            $("#HomeOfficeLocation").hide();
            $("#OtherReasonLocation").hide();
            setTrueOptionLocation('NewAsignationOk', true);
            clearFieldsLocation();
            emptyFieldObjectLocation(id);
            setIconDefaultField();
            $("#div_alert_opcion_locacion").hide();
            $("#txt_validacionOpcionLocation").html("");
            break;
        case "2":
            $("#NewAssignationLocation").hide();
            $("#VisitServiceClientLocation").show();
            $("#HomeOfficeLocation").hide();
            $("#OtherReasonLocation").hide();
            setTrueOptionLocation('VisitServiceClientOk', true);
            clearFieldsLocation();
            emptyFieldObjectLocation(id);
            setIconDefaultField();
            $("#div_alert_opcion_locacion").hide();
            $("#txt_validacionOpcionLocation").html("");
            break;
        case "3":
            $("#NewAssignationLocation").hide();
            $("#VisitServiceClientLocation").hide();
            $("#HomeOfficeLocation").show();
            $("#OtherReasonLocation").hide();
            setTrueOptionLocation('HomeOfficeOk', true);
            clearFieldsLocation();
            emptyFieldObjectLocation(id);
            setIconDefaultField();
            $("#div_alert_opcion_locacion").hide();
            $("#txt_validacionOpcionLocation").html("");
            break;
        case "4":
            $("#NewAssignationLocation").hide();
            $("#VisitServiceClientLocation").hide();
            $("#HomeOfficeLocation").hide();
            $("#OtherReasonLocation").hide();
            setTrueOptionLocation('BackOfficeOk', true);
            clearFieldsLocation();
            emptyFieldObjectLocation(id);
            setIconDefaultField();
            $("#div_alert_opcion_locacion").hide();
            $("#txt_validacionOpcionLocation").html("");
            break;
        case "5":
            $("#NewAssignationLocation").hide();
            $("#VisitServiceClientLocation").hide();
            $("#HomeOfficeLocation").hide();
            $("#OtherReasonLocation").show();
            setTrueOptionLocation('OtherReasonOk', true);
            clearFieldsLocation();
            emptyFieldObjectLocation(id);
            setIconDefaultField();
            $("#div_alert_opcion_locacion").hide();
            $("#txt_validacionOpcionLocation").html("");
            break;
    }
});

//Evento que valida que se haya cargado un archivo en el control
$("#file").change(function () {
    $("#validate_archivoTime").html("<i class='fa fa-check fa-2x position' style='color:green'></i>");
    var inpute = document.getElementById("file");
    if (inpute.files.length > 0) {
        var extencion = inpute.files[0].type;
        var name = inpute.files[0].name;
        var size = inpute.files[0].size;
        obj.Time.MedicalConsultationFileExtension = extencion;
        obj.Time.MedicalConsultationFileName = name;
        obj.Time.MedicalConsultationFileSize = size;
        var archivo = inpute.files[0];
        var lector = new FileReader();
        lector.addEventListener(
            "load",
            function (evento) {
                //document.getElementById("contenido-archivo").value = window.btoa(evento.target.result);
                document.getElementById("contenido-archivo").value = evento.target.result;
            }, false
        );
        lector.readAsDataURL(archivo);

    }
    else {
        $("#validate_archivoTime").html("<i class='fa fa-times fa-2x position' style='color:red'></i>");
    }
});

//**************************************************************************************//
//*********************************** End Events Methods *******************************//
//**************************************************************************************//

//*********************************************************************************//
//*********************************** Start Methods *******************************//
//*********************************************************************************//

function loadFile() {
    var inpute = document.getElementById("file");
    if (inpute.files.length > 0) {
        var archivo = inpute.files[0];
        var lector = new FileReader();
        lector.addEventListener(
            "load",
            function (evento) {
                document.getElementById("contenido-archivo").value = evento.target.result;
                //base64 = window.btoa(evento.target.result);
                obj.Time.MedicalConsultation = evento.target.result;
            }, false
        );
        lector.readAsDataURL(archivo);
        base64 = lector.result;
    }
}

//Metodo que consume una api para recuperar la ubicacion obteniendo en especial los valores de latitude y longitude
function GetLocation() {
    try {
        $.getJSON('https://ipapi.co/json/', function (response) {
            obj.IP = response.ip;
            $("#txt_ip").html(obj.IP);          
        }).fail(function () {
            alert("No pudo obtener la ubicación actual");
        }).catch(function (jqXHR, textStatus, errorThrown) {
            console.error(jqXHR);
            console.error(textStatus);
            console.error(errorThrown);
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            $("#txt_location").html("Geolocation is not supported by this browser.");
        }
    }
    catch (e) {
        obj.Latitude = '';
        obj.Longitude = '';

    }
    return obj;
}

//Metodo que recupera la ubicación actual del usuario, mostrando latitud y longitude
function showPosition(position) {
    obj.Latitude = position.coords.latitude;
    obj.Longitude = position.coords.longitude;
    $("#txt_location").html(position.coords.latitude + ", " + position.coords.longitude);
}

//Metodo que recupera y evalua en caso de existir un error, o no poder recuperar la ubicación
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            $("#txt_location").html("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            $("#txt_location").html("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            $("#txt_location").html("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            $("#txt_location").html("An unknown error occurred.");
            break;
    }
}

//Metodo que consume una api para obtener la ip publica del cliente
function GetPublicIP() {
    $.getJSON('https://api.ipify.org/?format=json', function (response) {
        obj.IP = response.ip;
        $("#txt_ip").html(obj.IP);
    }).catch(function (jqXHR, textStatus, errorThrown) {
        console.error(jqXHR);
        console.error(textStatus);
        console.error(errorThrown);
    });
}

function GetDateNew() {
    $.getJSON(URL + 'GetDateNow', function (response) {
        objDate.IsSuccess = response.IsSuccess;
        objDate.Message = response.Message;
        objDate.Date = response.Result;
    });
}

function getBase64File(files, obj) {
    var reader = new FileReader();
    reader.addEventListener("load",
        function (evento) {
            document.getElementById("contenido-archivo").value = window.btoa(evento.target.result);
        }, false);
    reader.readAsDataURL(files);
}

function ReadFile() {
    var inpute = document.getElementById("file");
    if (inpute.files.length > 0) {
        var archivo = inpute.files[0];
        var lector = new FileReader();
        lector.addEventListener(
            "onload",
            function (evento) {
                document.getElementById("contenido-archivo").value = evento.target.result;
                //base64 = window.btoa(evento.target.result);
                obj.Time.MedicalConsultation = window.btoa(evento.target.result);
            }, false
        );
        lector.readAsDataURL(archivo);
        base64 = lector.result;
    }
}

//valida y establece los iconos de "success" o "error" dependiendo de la validación
//fa-check-> en caso que el campo cumpla con la validación
//fa-times-> en caso que el campo no cumpla con la validación
function validateFieldEmptyIcon(id, div) {
    var text = $("#" + id).val();
    if (text != "") {
        $("#" + div).html("<i class='fa fa-check fa-2x position' style='color:green'></i>");
    }
    else {
        $("#" + div).html("<i class='fa fa-times fa-2x position' style='color:red'></i>");
    }
}


//establece los iconos de "info" en los campos de motivos al cargar la pagina
//fa-info-circle-> establece el icono de info, usado para indicar que es requerido
function fieldEmptyIcon(id, div) {
    if (id == "file") {
        $("#" + div).html("<i class='fa fa-info-circle fa-2x position' style='color:blue'></i>");
        return;
    }
    var text = $("#" + id).val();
    if (text == "") {
        $("#" + div).html("<i class='fa fa-info-circle fa-2x position' style='color:blue'></i>");
    }
}

//se limpia los campos de texto para el caso de motivos de cambio de ubicación
function clearFieldsLocation() {
    $("#txt_nuevaAsignacion").val("");
    $("#txt_clienteLocation").val("");
    $("#HomeOffice").val("");
    $("#txt_otroMotivoLocation").val("");
}

//se limpia los campos de texto para el caso de hora fuera del limite
function clearFieldsTime() {
    $("#txt_clienteTime").val("");
    $("#txt_incidentePersonal").val("");
    $("#CompensacionHoras").val("");
    $("#txt_otroMotivoTime").val("");
}

//establece con valor boleano true la opción seleccionada y las demas con false, para el caso de motivos de cambio de ubicación
function setTrueOptionLocation(name, boolean) {
    switch (name) {
        case "NewAsignationOk":
            obj.Location.NewAsignationOk = boolean;
            obj.Location.VisitServiceClientOk = false;
            obj.Location.HomeOfficeOk = false;
            obj.Location.BackOfficeOk = false;
            obj.Location.OtherReasonOk = false;
            break;
        case "VisitServiceClientOk":
            obj.Location.NewAsignationOk = false;
            obj.Location.VisitServiceClientOk = boolean;
            obj.Location.HomeOfficeOk = false;
            obj.Location.BackOfficeOk = false;
            obj.Location.OtherReasonOk = false;
            break;
        case "HomeOfficeOk":
            obj.Location.NewAsignationOk = false;
            obj.Location.VisitServiceClientOk = false;
            obj.Location.HomeOfficeOk = boolean;
            obj.Location.BackOfficeOk = false;
            obj.Location.OtherReasonOk = false;
            break;
        case "BackOfficeOk":
            obj.Location.NewAsignationOk = false;
            obj.Location.VisitServiceClientOk = false;
            obj.Location.HomeOfficeOk = false;
            obj.Location.BackOfficeOk = boolean;
            obj.Location.OtherReasonOk = false;
            break;
        case "OtherReasonOk":
            obj.Location.NewAsignationOk = false;
            obj.Location.VisitServiceClientOk = false;
            obj.Location.HomeOfficeOk = false;
            obj.Location.BackOfficeOk = false;
            obj.Location.OtherReasonOk = boolean;
            break;
        default:
            obj.Location.NewAsignationOk = false;
            obj.Location.VisitServiceClientOk = false;
            obj.Location.HomeOfficeOk = false;
            obj.Location.BackOfficeOk = false;
            obj.Location.OtherReasonOk = false;
            break;
    }
}

//establece con valor boleano true la opción seleccionada y las demas con false, para el caso de hora fuera del limite
function setActiveOptionTime(name, boolean) {
    switch (name) {
        case "VisitServiceClientOk":
            obj.Time.VisitServiceClientOk = boolean;
            obj.Time.MedicalConsultationOk = false;
            obj.Time.PersonalIncidentOk = false;
            obj.Time.CompensationHoursOk = false;
            obj.Time.OtherReasonOk = false;
            break;
        case "MedicalConsultationOk":
            obj.Time.VisitServiceClientOk = false;
            obj.Time.MedicalConsultationOk = boolean;
            obj.Time.PersonalIncidentOk = false;
            obj.Time.CompensationHoursOk = false;
            obj.Time.OtherReasonOk = false
            break;
        case "PersonalIncidentOk":
            obj.Time.VisitServiceClientOk = false;
            obj.Time.MedicalConsultationOk = false;
            obj.Time.PersonalIncidentOk = boolean;
            obj.Time.CompensationHoursOk = false;
            obj.Time.OtherReasonOk = false;
            break;
        case "CompensationHoursOk":
            obj.Time.VisitServiceClientOk = false;
            obj.Time.MedicalConsultationOk = false;
            obj.Time.PersonalIncidentOk = false;
            obj.Time.CompensationHoursOk = boolean;
            obj.Time.OtherReasonOk = false;
            break;
        case "OtherReasonOk":
            obj.Time.VisitServiceClientOk = false;
            obj.Time.MedicalConsultationOk = false;
            obj.Time.PersonalIncidentOk = false;
            obj.Time.CompensationHoursOk = false;
            obj.Time.OtherReasonOk = boolean;
            break;
        default:
            obj.Time.VisitServiceClientOk = false;
            obj.Time.MedicalConsultationOk = false;
            obj.Time.PersonalIncidentOk = false;
            obj.Time.CompensationHoursOk = false;
            obj.Time.OtherReasonOk = false;
            break;
    }
}

//limpia las propiedades del objeto objLocation
function emptyFieldObjectLocation(id) {
    obj.Location.NewAsignation = "";
    obj.Location.HomeOffice = "";
    obj.Location.OtherReason = "";
    obj.Location.IdLocationReason = id;
}

//limpia las propiedades del objeto objTime
function emptyFieldObjectTime(id) {
    obj.Time.VisitServiceClient = "";
    obj.Time.PersonalIncident = "";
    obj.Time.CompensationHours = "";
    obj.Time.OtherReason = ""
    obj.Time.IdTimeReason = id;
}

//Metodo que agrupa los metodos "fieldEmptyIcon" en cada caso de los campos.
function setIconDefaultField() {
    fieldEmptyIcon('file', 'validate_archivoTime');
    fieldEmptyIcon('CompensacionHoras', 'validate_compensacionHoras');
    fieldEmptyIcon('HomeOffice', 'validate_HomeOffice');
    fieldEmptyIcon('txt_nuevaAsignacion', 'validate_nuevaAsignacion');
    fieldEmptyIcon('txt_otroMotivoLocation', 'validate_otroMotivoLocation');
    fieldEmptyIcon('txt_otroMotivoTime', 'validate_otroMotivoTime');
    fieldEmptyIcon('txt_incidentePersonal', 'validate_incidentePersonal');
    fieldEmptyIcon('txt_clienteLocation', 'validate_clienteLocation');
    fieldEmptyIcon('txt_clienteTime', 'validate_clienteTime');
}

//*******************************************************************************//
//*********************************** End Methods *******************************//
//*******************************************************************************//


//**************************************************************************************//
//*********************************** Start Ajax Methods *******************************//
//**************************************************************************************//

//Metodo usado para registro de entrada
function SetTimeEntry(url, data) {
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function () {
            $("#div_loading").show();
        },
        success: function (response) {
            console.log(response);          
            $("#div_alertsuccess").hide();
            $("#txt_mensajeEntradaRegistro").html("");
            //valida la respuesta del servidor
            if (response.IsSuccess) {
                $("#div_MotivoHoraEntrada").hide();
                $("#div_MotivoUbicacion").hide();
                $("#div_alertsuccess").show();
                $("#div_alertsuccess").removeClass("alert alert-danger").addClass("alert alert-success");
                $("#txt_mensajeEntradaRegistro").html(response.Message);
                $("#message").html("");
                $('#btn_entrar').attr('disabled', 'disabled');
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                obj.entryOk = response.entryOk;
                $("#message").html("<h5 class='text-info'>Para continuar de clic en el botón de registro de entrada</h5>");
                $("#btn_entrar").removeAttr('disabled');
            }
            if (!response.LocationOK) {
                $("#txt_mensajeLocation").html(response.Message);
                $("#div_MotivoUbicacion").show();
                obj.LocationOk = false;
                $("#btn_entrar").removeAttr('disabled');
            }
            else {
                $("#div_MotivoUbicacion").hide();
            }

            if (!response.TimeOk) {
                $("#txt_mensajeTime").html(response.Message);
                $("#div_MotivoHoraEntrada").show();
                obj.TimeOk = false;
                $("#btn_entrar").removeAttr('disabled');
            }
            else {
                $("#div_MotivoHoraEntrada").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
            $("#btn_entrar").removeAttr('disabled');
        },
        complete: function () {
            $("#div_loading").hide();
        }
    });
}


//Metodo usado para registro de salida
function SetTimeDeparture(url, data) {
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function () {
            $("#div_loading").show();
        },
        success: function (response) {
            console.log(response);
            $("#div_alertsuccess").hide();
            $("#txt_mensajeEntradaRegistro").html("");
            //valida la respuesta del servidor
            if (response.IsSuccess) {
                $("#div_MotivoHoraEntrada").hide();
                $("#div_MotivoUbicacion").hide();
                $("#div_alertsuccess").show();
                $("#txt_mensajeEntradaRegistro").html(response.Message);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                obj.entryOk = response.entryOk;
                if (!obj.entryOk) {
                    $("#div_alertsuccess").show();
                    $("#div_alertsuccess").removeClass('alert alert-success').addClass('alert alert-danger');
                    $("#txt_mensajeEntradaRegistro").html("No se puede registrar la salida sin haber registrado la entrada primero.");
                }
            }
            if (!response.LocationOK) {
                $("#txt_mensajeLocation").html(response.Message);
                $("#div_MotivoUbicacion").show();
                obj.LocationOk = false;
            }
            else {
                $("#div_MotivoUbicacion").hide();
            }

            if (!response.TimeOk) {
                $("#txt_mensajeTime").html(response.Message);
                $("#div_MotivoHoraEntrada").show();
                obj.TimeOk = false;
            }
            else {
                $("#div_MotivoHoraEntrada").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {
            $("#div_loading").hide();
        }
    });
}


//Metodo usado para validar si es cumpleaños del usuario y de serlo mostrar mensaje de Felicitación
function GetBirthdayUser(url) {
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        beforeSend: function () {
        },
        success: function (response) {
            console.log(response);
            if (response.IsSuccess) {
                $("#txt_messageBirthday").html("" +
                    "<div class='alert alert-success' role='alert'>" +
                    "<span><b>" + response.Result + "</b></span>" +
                    "</div >");
            }           
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {
        }
    });
}


//**************************************************************************************//
//*********************************** End Ajax Methods *********************************//
//**************************************************************************************//



//*********************************************************************************************//
//*********************************** Start Init Load Methods *********************************//
//*********************************************************************************************//

//----------------------- Clock Event -----------------------//
$(function () {
    var actualizarHora = function () {
        GetDateNew();
        if (objDate.IsSuccess) {
            var date = objDate.Date.split('T')[0];
            var time = objDate.Date.split('T')[1];
            var dServer = date.split('-');
            var tServer = time.split(':');
            var fecha = new Date(parseInt(dServer[2]), parseInt(dServer[1]) - 1, parseInt(dServer[0]), parseInt(tServer[0]), parseInt(tServer[1]), parseInt(tServer[2]), 0);

            var hora = fecha.getHours();
            var minutos = fecha.getMinutes();
            var segundos = fecha.getSeconds();
            var diaSemana = fecha.getDay();
            var dia = fecha.getDate();
            var mes = fecha.getMonth();
            var anio = fecha.getFullYear();
            var ampm;

            var $pHoras = $("#horas"),
                $pSegundos = $("#segundos"),
                $pMinutos = $("#minutos"),
                $pAMPM = $("#ampm"),
                $pDiaSemana = $("#diaSemana"),
                $pDia = $("#dia"),
                $pMes = $("#mes"),
                $pAnio = $("#anio");
            var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

            $pDiaSemana.text(semana[diaSemana]);
            $pDia.text(dia);
            $pMes.text(meses[mes]);
            $pAnio.text(anio);
            if (hora >= 12) {
                hora = hora - 12;
                ampm = "PM";
            } else {
                ampm = "AM";
            }
            if (hora == 0) {
                hora = 12;
            }
            if (hora < 10) { $pHoras.text("0" + hora) } else { $pHoras.text(hora) };
            if (minutos < 10) { $pMinutos.text("0" + minutos) } else { $pMinutos.text(minutos) };
            if (segundos < 10) { $pSegundos.text("0" + segundos) } else { $pSegundos.text(segundos) };
            $pAMPM.text(ampm);
        }
    };
    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
});

//*******************************************************************************************//
//*********************************** End Init Load Methods *********************************//
//*******************************************************************************************//