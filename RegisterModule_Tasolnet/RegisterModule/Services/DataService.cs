﻿using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Services
{
    public class DataService
    {
        public string GetMessageBirthday(string _birthday, DateTime dateNow)
        {
            string message = string.Empty;
            //Fecha del dia presente
            var dNow = dateNow;
            var dayWeekNow = dNow.DayOfWeek;
            var dayNow = dNow.Day;
            var monthNow = dNow.Month;

            //Fecha de cumpleaños del usuario
            var Birthdate = _birthday;
            if (!string.IsNullOrEmpty(Birthdate))
            {
                //Formato en como se recupera la fecha -> mm/dd/yyyy 00:00:00
                var dBirthday = Birthdate.ConvertStringToDateTime();
                var dayWeekBirthdate = dBirthday.DayOfWeek;
                var dayBirthdate = dBirthday.Day;
                var monthBirthdate = dBirthday.Date.Month;

                //Se valida que el mes de cumpleaños sea el mismo que el mes presente
                if (monthNow == monthBirthdate)
                {
                    //Se valida si el dia es menor o igual al dia presente
                    //En caso de ser menor se valida si el dia de hoy es lunes, siendo asi, se valida si 
                    //el cumpleaños fue en fin de semana anterior.
                    if (dayBirthdate <= dayNow)
                    {
                        //Se valida que el dia presente sea lunes, para validar si fue en fin de semana
                        if (dayWeekNow == DayOfWeek.Monday)
                        {
                            if (dayBirthdate == dayNow)
                                return MessageBirthday.Message;
                            //Se le resta 1 dia al dia presente para obtener el dia DOMINGO
                            var saturdayDayBirthday = dayNow - 1;
                            //Se le resta 2 dia al dia presente para obtener el dia SABADO
                            var sundayDayBirthday = dayNow - 2;

                            //Se valida si el cumpleños fue en algun dia del fin de semana
                            if (dayBirthdate == saturdayDayBirthday || dayBirthdate == sundayDayBirthday)
                                message = MessageBirthday.Message;
                        }
                        else
                            message = MessageBirthday.Message;
                    }
                    else
                        message = string.Empty;
                }
            }
            return message;
        }
    }
}