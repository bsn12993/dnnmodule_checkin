﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public static class MessageBirthday
    {
        public static readonly string Message =
            @"Para nosotros es un gusto contar contigo y a nombre de nuestra compañía, en el día de tu cumpleaños, te deseamos dar nuestras felicitaciones y nuestros más sinceros deseos de que tus metas se hagan realidad.[[br]]
            Como parte de nuestra política de beneficios, el día 15 de este mes recibes una gratificación adicional en tu tarjeta de vales de despensa.";
    }
}