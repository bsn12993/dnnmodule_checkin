﻿using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Util;
using DotNetNuke.Web.Api;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DotNetNuke.Entities.Users;
using Christoc.Modules.RegisterModule.ViewModels;
using Christoc.Modules.RegisterModule.Services;
using System.Globalization;

namespace Christoc.Modules.RegisterModule.Models
{
    public class ModuleTaskController : DnnApiController
    {
        DateTime Now = DateTime.Now.Date;
        TimeSpan noTime = new TimeSpan(0, 0, 0);
        TimeSpan EntryTimeStandard = new TimeSpan(8, 0, 59);
        TimeSpan _entryTime = DateTime.Now.TimeOfDay;
        TimeSpan _departureTime = DateTime.Now.TimeOfDay;
        CheckInService checkInService = new CheckInService();
        private double _latitudRound = 0;
        private double _longitudeRound = 0;

        public ModuleTaskController()
        {
        }

        /// <summary>
        /// Registra Hora de Entrada del Usuario
        /// </summary>
        /// <param name="entryTime">hora de entrada obtenida del cliente</param>
        /// <returns>Objeto Response</returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage EntryTime([FromBody]CheckInVM model)
        {
            Response Response = new Response();
            string location = string.Empty;
            var idmodule = (CurrentModule.GetInstance().ModuleID == 0) ?
                UtilHelper.GetModuleID() :
                CurrentModule.GetInstance().ModuleID;
            var entryTime = UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("HoraEntrada");
            if (string.IsNullOrEmpty(model.Latitude) || string.IsNullOrEmpty(model.Longitude))
                location = string.Empty;
            else
                location = string.Format("{0}, {1}", model.Latitude, model.Longitude);
            if (!string.IsNullOrEmpty(entryTime)) EntryTimeStandard = TimeSpan.Parse(entryTime);
            var user = UserController.Instance.GetCurrentUserInfo().UserID;
            var userController = UserController.Instance.GetCurrentUserInfo();
            try
            {
                //Se recupera el ultimo registro del usuario en sesión para validar la ip
                var recordYesterday = checkInService.GetUserById(user);
                if (recordYesterday != null)
                {
                    //Se valida si el dia presente es el cumpleaños del usuario
                    string propertyBirthday =
                        (!string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday")))
                        ? UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday") : string.Empty;
                    var message = new DataService().GetMessageBirthday(propertyBirthday, DateTime.Now.Date);

                    //Si la variable "message" es "" significa que no es cumpleaños del usuario
                    //Si la variable "message" es diferente de "", osea que contiene texto, el texto es el 
                    //mensaje que se mostrara como felicitación
                    if (string.IsNullOrEmpty(message))
                    {
                        //se valida la ip del registro anterior con el actual
                        //Si es la misma ip continua en la condición
                        //Si no es la misma, le envia un mensaje al usuario para que ingrese una asignacion

                        //Se recupera la ubicacion del dia anterior
                        var loc = recordYesterday.Location.Split(',');
                        if (!string.IsNullOrEmpty(loc[0]) && !string.IsNullOrEmpty(loc[1]))
                        {
                            //Se recurpa dla ubicacion del dia/registro anterior
                            var lat = string.Format("{0}.{1}", (loc[0].Split('.')[0]), (loc[0]).Split('.')[1].Substring(0, 3));
                            var lon = string.Format("{0}.{1}", (loc[1].Split('.')[0]), (loc[1]).Split('.')[1].Substring(0, 3));

                            //Se recupera la ubicacion del dia presente
                            var ltd = string.Format("{0}.{1}", model.Latitude.Split('.')[0], model.Latitude.Split('.')[1].Substring(0, 3));
                            _latitudRound = double.Parse(ltd, CultureInfo.InvariantCulture);
                            var lng = string.Format("{0}.{1}", model.Longitude.Split('.')[0], model.Longitude.Split('.')[1].Substring(0, 3));
                            //Se quita el signo negativo (-) de la longitud recuperada del dia presente
                            lng = lng.Trim();
                            lng = lng.Substring(1, lng.Length - 1);
                            _longitudeRound = double.Parse(lng, CultureInfo.InvariantCulture);

                            //Rango de latitud Min y Max 
                            var latitudRoundMin = double.Parse(lat, CultureInfo.InvariantCulture) - 0.008;
                            var latitudRoundMax = double.Parse(lat, CultureInfo.InvariantCulture) + 0.008;

                            //Rango de longitud Min y Max 
                            //En el caso de la longitud de quita el signo negativo (-) para poder sacar el rango
                            //y que el signo no altere la validación
                            lon = lon.Trim();
                            lon = lon.Substring(1, lon.Length - 1);
                            var longitudeRoundMin = double.Parse(lon, CultureInfo.InvariantCulture) - 0.008;
                            var longitudeRoundMax = double.Parse(lon, CultureInfo.InvariantCulture) + 0.008;

                            //Validacion de rango de latitude y longitude
                            if ((_latitudRound >= latitudRoundMin && _latitudRound <= latitudRoundMax) &&
                                (_longitudeRound >= longitudeRoundMin && _longitudeRound <= longitudeRoundMax))
                            {
                                location = string.Format("{0}, {1}", model.Latitude, model.Longitude);
                            }
                            else
                            {
                                //se valida que se haya seleccionado alguna opción del motivo de cambio de ubicación
                                //retornando un valor true|false
                                var validateLocation = UtilHelper.ValidateLocationOption(model.Location);
                                if (!validateLocation)
                                {
                                    Response.IsSuccess = false;
                                    Response.Message = "Hemos identificado que el día de hoy estas registrándote en otra ubicación, por favor nos compartes la razón";
                                    Response.LocationOK = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }

                                //se valida que se el campo en base a la opcion seleccionada de cambio de ubicacion
                                //retornando un valor true|false
                                var validateFieldsLocation = UtilHelper.ValidateFieldLocation(model.Location);
                                if (!(bool)validateFieldsLocation[0])
                                {
                                    Response.IsSuccess = (bool)validateFieldsLocation[0];
                                    Response.Message = (string)validateFieldsLocation[1];
                                    Response.LocationOK = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }
                            }
                        }


                        //validateEntryTime == 0 -> indica que el valor es igual al esperado
                        //validateEntryTime == -1 indica que el valor es menor al esperdado
                        //validateEntryTime == 1 indica que el valor es mayor al esperdado
                        var validateEntryTimeStandard = model.entryTime.CompareTo(EntryTimeStandard);
                        if (validateEntryTimeStandard == 1)
                        {
                            //se valida que se haya seleccionado alguna opción del motivo de hora de entrada
                            //retornando un valor true|false
                            var validateTime = UtilHelper.ValidateTimeOption(model.Time);
                            if (!validateTime)
                            {
                                Response.IsSuccess = false;
                                Response.Message = "Hemos identificado que estas fuera del horario estándar de ingreso, por favor nos compartes el motivo";
                                Response.TimeOk = false;
                                Response.entryOk = false;
                                return Request.CreateResponse(HttpStatusCode.OK, Response);
                            }

                            //se valida que se el campo en base a la opcion seleccionada de hora de entrada
                            //retornando un valor true|false
                            var validateFieldsTime = UtilHelper.ValidateFieldTime(model.Time);
                            if (!(bool)validateFieldsTime[0])
                            {
                                Response.IsSuccess = (bool)validateFieldsTime[0];
                                Response.Message = (string)validateFieldsTime[1];
                                Response.TimeOk = false;
                                Response.entryOk = false;
                                return Request.CreateResponse(HttpStatusCode.OK, Response);
                            }

                            //Se valida si cuenta con un archivo adjunto
                            if (model.Time.MedicalConsultationOk)
                            {
                                var fileExtensions = checkInService.GetFileExtensions();
                                if (!fileExtensions.IsSuccess)
                                {
                                    Response.IsSuccess = fileExtensions.IsSuccess;
                                    Response.Message = fileExtensions.Message;
                                    Response.TimeOk = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }
                                var extensions = (FileExtension)fileExtensions.Result;
                                if (!UtilHelper.ValidateExtension(extensions.AllowedAttachmentFileExtensions, model.Time.MedicalConsultationFileExtension))
                                {
                                    Response.IsSuccess = false;
                                    Response.Message = "El formato del archivo seleccionado no es un formato permitido";
                                    Response.TimeOk = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }
                                else
                                {
                                    model.Time.MedicalConsultationFileName = UtilHelper
                                        .SaveFileTemp(model.Time.MedicalConsultation, idmodule, model.Time.MedicalConsultationFileName);
                                }
                            }
                        }
                    }


                    //Registra la entrada por primera vez del dia
                    var insertEntryTime = checkInService.SetEntryTime(new Register
                    {
                        Entry = _entryTime,
                        IdUser = user,
                        Date = Now,
                        IpAddress = model.IP,
                        Assignment = (!string.IsNullOrEmpty(model.Asignation)) ? model.Asignation : UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("AssignedTo"),
                        Location = location,
                        IDEmployee = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("IDEmployee"))
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("IDEmployee")),
                        TimeOk = Convert.ToInt32(model.TimeOk),
                        LocationOk = Convert.ToInt32(model.LocationOk),
                        NameClientTime = model.Time.VisitServiceClientTime,
                        NameSupervisorTime = model.Time.CompensationHours,
                        PersonalIncidentTime = model.Time.PersonalIncident,
                        OtherReasonTime = model.Time.OtherReason,
                        MedicalVoucherFile = model.Time.MedicalConsultation,
                        IdTimeReason = model.Time.IdTimeReason,
                        NameClientLocation = (string.IsNullOrEmpty(model.Location.VisitServiceClientLocation) ?
                                    model.Location.NewAsignation : model.Location.VisitServiceClientLocation),
                        NameSupervisorLocation = model.Location.HomeOffice,
                        OtherReasonLocation = model.Location.OtherReason,
                        IdLocationReason = model.Location.IdLocationReason,
                        NameUser = (!string.IsNullOrEmpty(userController.FirstName))
                            ? userController.FirstName : string.Empty,
                        LastNameUser= (!string.IsNullOrEmpty(userController.LastName))
                            ? userController.LastName : string.Empty,
                        EmailUser = (!string.IsNullOrEmpty(userController.Email))
                            ? userController.Email : string.Empty
                    }, new Ticket
                    {
                        ModuleID = idmodule,
                        ChannelID = 0,
                        ReporterID = 1,
                        Email = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Email)
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().Email),
                        Name = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().FirstName)
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().FirstName),
                        Phone = string.Empty,
                        Subject = "",
                        Message = "",
                        StatusID = 1,
                        CreatedOn = DateTime.Now.Date
                    }, model);

                    //Valida respuesta
                    if (insertEntryTime.IsSuccess)
                    {
                        var entry = TimeHelper.ConvertTime(_entryTime);
                        Response.IsSuccess = true;
                        Response.Message = insertEntryTime.Message + " " + entry;
                    }
                    else
                    {
                        Response.IsSuccess = insertEntryTime.IsSuccess;
                        Response.Message = insertEntryTime.Message;
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Response.Message);
                    }
                }

                //no existe registro del dia anterior significa que es la primera vez que el usuario registra entrada 
                //y no hay registras anteriores con que comparar la ip
                else
                {
                    //Se valida si el dia presente es el cumpleaños del usuario
                    string propertyBirthday =
                        (!string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday")))
                        ? UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday") : string.Empty;
                    var message = new DataService().GetMessageBirthday(propertyBirthday, DateTime.Now.Date);

                    if (string.IsNullOrEmpty(message))
                    {
                        //validateEntryTime == 0 -> indica que el valor es igual al esperado
                        //validateEntryTime == -1 indica que el valor es menor al esperdado
                        //validateEntryTime == 1 indica que el valor es mayor al esperdado
                        var validateEntryTimeStandard = model.entryTime.CompareTo(EntryTimeStandard);
                        if (validateEntryTimeStandard == 1)
                        {
                            var validateTime = UtilHelper.ValidateTimeOption(model.Time);
                            if (!validateTime)
                            {
                                Response.IsSuccess = false;
                                Response.Message = "Hemos identificado que estas fuera del horario estándar de ingreso, por favor nos compartes el motivo";
                                Response.TimeOk = false;
                                Response.entryOk = false;
                                return Request.CreateResponse(HttpStatusCode.OK, Response);
                            }

                            //se valida que se el campo en base a la opcion seleccionada de hora de entrada
                            //retornando un valor true|false
                            var validateFieldsTime = UtilHelper.ValidateFieldTime(model.Time);
                            if (!(bool)validateFieldsTime[0])
                            {
                                Response.IsSuccess = (bool)validateFieldsTime[0];
                                Response.Message = (string)validateFieldsTime[1];
                                Response.TimeOk = false;
                                Response.entryOk = false;
                                return Request.CreateResponse(HttpStatusCode.OK, Response);
                            }

                            //Se valida si cuenta con un archivo adjunto
                            if (model.Time.MedicalConsultationOk)
                            {
                                var fileExtensions = checkInService.GetFileExtensions();
                                if (!fileExtensions.IsSuccess)
                                {
                                    Response.IsSuccess = fileExtensions.IsSuccess;
                                    Response.Message = fileExtensions.Message;
                                    Response.TimeOk = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }
                                var extensions = (FileExtension)fileExtensions.Result;
                                if (!UtilHelper.ValidateExtension(extensions.AllowedAttachmentFileExtensions, model.Time.MedicalConsultationFileExtension))
                                {
                                    Response.IsSuccess = false;
                                    Response.Message = "El formato del archivo seleccionado no es un formato permitido";
                                    Response.TimeOk = false;
                                    Response.entryOk = false;
                                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                                }
                                else
                                {
                                    model.Time.MedicalConsultationFileName = UtilHelper
                                        .SaveFileTemp(model.Time.MedicalConsultation, idmodule, model.Time.MedicalConsultationFileName);
                                }
                            }
                        }
                    }

                    var insertEntryTime = checkInService.SetEntryTime(new Register
                    {
                        Entry = _entryTime,
                        IdUser = user,
                        Date = Now,
                        IpAddress = model.IP,
                        Assignment = (!string.IsNullOrEmpty(model.Asignation)) ? model.Asignation : UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("AssignedTo"),
                        Location = location,
                        IDEmployee = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("IDEmployee"))
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("IDEmployee")),
                        TimeOk = Convert.ToInt32(model.TimeOk),
                        LocationOk = Convert.ToInt32(model.LocationOk),
                        NameClientTime = model.Time.VisitServiceClientTime,
                        NameSupervisorTime = model.Time.CompensationHours,
                        PersonalIncidentTime = model.Time.PersonalIncident,
                        OtherReasonTime = model.Time.OtherReason,
                        MedicalVoucherFile = model.Time.MedicalConsultation,
                        IdTimeReason = model.Time.IdTimeReason,
                        NameClientLocation = (string.IsNullOrEmpty(model.Location.VisitServiceClientLocation) ?
                                    model.Location.NewAsignation : model.Location.VisitServiceClientLocation),
                        NameSupervisorLocation = model.Location.HomeOffice,
                        OtherReasonLocation = model.Location.OtherReason,
                        IdLocationReason = model.Location.IdLocationReason,
                        NameUser = (!string.IsNullOrEmpty(userController.FirstName))
                            ? userController.FirstName : string.Empty,
                        LastNameUser = (!string.IsNullOrEmpty(userController.LastName))
                            ? userController.LastName : string.Empty,
                        EmailUser= (!string.IsNullOrEmpty(userController.Email))
                            ? userController.Email : string.Empty
                    }, new Ticket
                    {
                        ModuleID = idmodule,
                        ChannelID = 0,
                        ReporterID = 1,
                        Email = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Email)
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().Email),
                        Name = (string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().FirstName)
                            ? string.Empty : UserController.Instance.GetCurrentUserInfo().FirstName),
                        Phone = string.Empty,
                        Subject = "",
                        Message = "",
                        StatusID = 1,
                        CreatedOn = DateTime.Now.Date
                    }, model);
                    //Se valida respuesta
                    if (insertEntryTime.IsSuccess)
                    {
                        var entry = TimeHelper.ConvertTime(_entryTime);
                        Response.IsSuccess = true;
                        Response.Message = insertEntryTime.Message + "" + entry;
                    }
                    else
                    {
                        Response.IsSuccess = insertEntryTime.IsSuccess;
                        Response.Message = insertEntryTime.Message;
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Response.Message);
                    }
                }   
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message
                    + " - InnerException: " + e.InnerException
                    + " - Source: " + e.Source + " - StackTrace: " 
                    + e.StackTrace + " - TargetSite: " + e.TargetSite;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response);
        }

        /// <summary>
        /// Registra Hora de Salida del Usuario
        /// </summary>
        /// <param name="entryTime">hora de salida obtenida del cliente</param>
        /// <returns>Objeto Response</returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage DepartureTime([FromBody]TimeSpan departureTime)
        {
            Response Response = new Response();
            try
            {
                var user = UserController.Instance.GetCurrentUserInfo().UserID;
                var records = checkInService.GetUSerRecordsToday(user);
                if (records != null)
                {
                    // valida si la hora de salida ya existe 
                    // si la hora es = 0 -> indica que no hay hora registrada y realiza el registro, de lo contrario muestra un mensaje 
                    // indicando que la hora ya esta registrada
                    var validateDeparture = records.Departure.CompareTo(noTime);
                    if (validateDeparture == 0)
                    {
                        var insert = checkInService.SetDepartureTime(new Register
                        {
                            Departure = _departureTime,
                            IdUser = user
                        });
                        //se valida respuesta
                        if (insert.IsSuccess)
                        {
                            var departure = TimeHelper.ConvertTime(_departureTime);
                            var msg = string.Format("{0} {1}", insert.Message, departure);
                            Response.IsSuccess = true;
                            Response.Message = msg;
                        }
                        else
                        {
                            Response.IsSuccess = insert.IsSuccess;
                            Response.Message = insert.Message;
                        }
                    }
                    else
                    {
                        Response.IsSuccess = true;
                        Response.Message = "Ya se encuentra registrada la salida";
                    }
                }
                else
                {
                    Response.IsSuccess = false;
                    Response.Message = "No se puede registrar la salida sin haber registrado la entrada primero.";
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response);
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetDateNow()
        {
            Response Response = new Response();
            try
            {
                Response.IsSuccess = true;
                Response.Message = "Se recupero la fecha actual del servidor";
                Response.Result = DateTime.Now.ToString("dd-MM-yyyyTHH:mm:ss");
                return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
            }
            catch(Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = string.Format("{0} - {1}", e.Message, "No se pudo recuperar la fecha actual del servidor");
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetBirthdayUser()
        {
            Response response = new Response();
            string propertyBirthday = string.Empty;
            string message = string.Empty;
            try
            {
                int iduser = UserController.Instance.GetCurrentUserInfo().UserID;
                propertyBirthday =
                (!string.IsNullOrEmpty(UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday")))
                ? UserController.Instance.GetCurrentUserInfo().Profile.GetPropertyValue("Birthday") : string.Empty;

                if (string.IsNullOrEmpty(propertyBirthday))
                {
                    response.IsSuccess = false;
                    response.Message = $"Usuario no tiene fecha de cumpleaños aún - {propertyBirthday}";
                    response.Result = null;
                    return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
                }
                //Se valida si registro el dia de su cumpleaños (ayer), para determinar si se muestr el mensaje
                //de felicitación el dia presente o no
                var service = checkInService.ValidateBirthdayUser(propertyBirthday, iduser);
                if (service.IsSuccess)
                {
                    message = new DataService().GetMessageBirthday(propertyBirthday, DateTime.Now.Date);
                    if (!string.IsNullOrEmpty(message))
                    {
                        message = message.Replace("[[br]]", "</br>");
                        response.IsSuccess = true;
                        response.Message = $"Usuario con cumpleaños- {propertyBirthday}";
                        response.Result = message;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = $"Usuario sin cumpleaños - {propertyBirthday}";
                        response.Result = null;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
                }

                message = new DataService().GetMessageBirthday(propertyBirthday, DateTime.Now.Date);
                if (!string.IsNullOrEmpty(message))
                {
                    message = message.Replace("[[br]]", "</br>");
                    response.IsSuccess = true;
                    response.Message = $"Usuario con cumpleaños- {propertyBirthday}";
                    response.Result = message;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = $"Usuario sin cumpleaños - {propertyBirthday}";
                    response.Result = null;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
            }
            catch(Exception e)
            {
                response.IsSuccess = false;
                response.Message = e.Message + " - " + propertyBirthday;
                response.Result = null;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, response, "application/json");
            }
        }
    }
}