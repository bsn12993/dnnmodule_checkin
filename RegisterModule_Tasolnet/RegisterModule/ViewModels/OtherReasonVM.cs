﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class OtherReasonVM
    {
        public string Description { get; set; }
    }
}