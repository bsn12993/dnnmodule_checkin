﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Util
{
    public static class Extensions
    {
        /// <summary>
        /// Metodo extención utilizado para convertir una fecha en string a DateTime, invocando el metodo 
        /// como extencion del tipo de dato
        /// </summary>
        /// <param name="value">fecha en string</param>
        /// <returns>fecha datetime</returns>
        public static DateTime ConvertStringToDateTime(this string value)
        {
            var date = value.Split(new Char[] { ' ' }, 2).First();
            //var date = value.Split(' ')[0];
            var m = date.Split('/')[0];
            var d = date.Split('/')[1];
            var y = date.Split('/')[2];
            return new DateTime(Convert.ToInt32(y), Convert.ToInt32(m), Convert.ToInt32(d));
        }
    }
}