﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.EntityModels
{
    [Table("chk_Register")]
    public class Register
    {
        [Key]
        [Column("id_register")]
        public int Register_Id { get; set; }
        [Required]
        [Column("id_user")]
        public int IdUser { get; set; }
        [Column("entry")]
        public TimeSpan Entry { get; set; }
        [Column("departure")]
        public TimeSpan Departure { get; set; }
        [Column("date")]
        public DateTime Date { get; set; }
        [Column("ip_address")]
        [MaxLength(30)]
        public string IpAddress { get; set; }
        [Column("assignment")]
        [MaxLength(50)]
        public string Assignment { get; set; }
        [Column("id_employee")]
        [MaxLength(15)]
        public string IDEmployee { get; set; }
        //longitude latitude
        [Column("location")]
        [MaxLength(50)]
        public string Location { get; set; }
        public int TimeOk { get; set; }

        public string NameClientTime { get; set; }
        public string NameSupervisorTime { get; set; }
        public string PersonalIncidentTime { get; set; }
        public string MedicalVoucherFile { get; set; }
        public string OtherReasonTime { get; set; }
        public int IdTimeReason { get; set; }

        public int LocationOk { get; set; }
        public string NameClientLocation { get; set; }
        public string NameSupervisorLocation { get; set; }
        public string OtherReasonLocation { get; set; }
        public int IdLocationReason { get; set; }

        public string NameUser { get; set; }
        public string LastNameUser { get; set; }
        public string EmailUser { get; set; }

        public string FullNameUser
        {
            get { return string.Format("{0} {1}", this.NameUser, this.LastNameUser); }
        }

        public Register()
        {
            this.Location = string.Empty;
            this.NameClientTime = string.Empty;
            this.NameSupervisorTime = string.Empty;
            this.PersonalIncidentTime = string.Empty;
            this.MedicalVoucherFile = string.Empty;
            this.OtherReasonTime = string.Empty;

            this.NameClientLocation = string.Empty;
            this.NameSupervisorLocation = string.Empty;
            this.OtherReasonLocation = string.Empty;

            this.NameUser = string.Empty;
            this.LastNameUser = string.Empty;
            this.EmailUser = string.Empty;
        }
    }
}