# DNNModule_CheckIn

# Modulo de Resgistro Entradas y Salida 

##Clonaci�n

###### Es importante mencionar que cuando se clone el proyecto la ubici�n sea dentro del sitio DNN dentro la carpeta **DesktopModules\MVC**.
###### Es importante que sea dentro del sitio del DNN ya que necesita referencias de DLL del proyecto general DNN, a continuaci�n se mencionan las DLL requeridas
- ** DotNetNuke.dll **
- ** DotNetNuke.Web.dll **
- ** DotNetNuke.Web.Mvc.dll **
- ** Microsoft.ApplicationBlocks.Data **
- ** System.Web.Http.dll **

##Conexi�n a Base de datos Local/Servidor

###### Se puede modificar los datos de la conexi�n a la base de datos accediendo a los siguientes archivos.
- DAL/DataAccess.cs: la variable **connectionString** contiene la conexi�n a la base de datos, bastaria con editarla con los datos correspondientes.
	- En esta clase se encuentra los metodos que realizan las consultas a la tabla, por medio de **ADO.NET**.
- Views/CheckIn/Index.cshtml: 
	- **/tasinet2/DesktopModules/RegisterModule/API/ModuleTask/EntryTime**: Registro de Entrada.
	- **/tasinet2/DesktopModules/RegisterModule/API/ModuleTask/DepartureTime**: Registro de Salida.
		-Para el servidor es colocar al inicio /tasinet2/ de manera local solo basta quitarsela en ambos casos.
	

##Creaci�n de tabla del modulo en DNN
 
###### Si se desea editar el script de la tabla, este se localiza en **Providers\DataProviders\SqlDataProvider** donde se encontraran los siguietes archivos.
- **00.00.01.SqlDataProvider**: este script se ejecuta cuando el modulo se instala, el cual crea la tabla **CREATE TABLE**.
- **Uninstall.SqlDataProvider**: este script se ejecuta cuando el modulo se desinstala el cual elimina la tabla ejecutando **DROP TABLE**.
 
##Petici�nes al Servidor
 
###### En el caso de los modulos desarrollados para DNN las peticiones hacia el Servidor/Controlador se realiza un poco diferente como conmumente se hace en una aplicacion web ASP.NET MVC normal.
###### En este caso la comunicaci�n o petici�n debe realizarse como un consumo a WebAPI. Los siguienetes archivos estan relacionados a esta configuraci�n.
- **Models/ModuleTaskController.cs**: Es el controlador que contiene los metodos los cuales retornan una respuesta de tipo **HttpResponseMessage** que hereda de **DnnApiController**, ambos metodos usan **HttpPost** para envio de datos al servidor.
- **Models/RouteMapper.cs**: Es la clase donde se configura el mapeo de contralador.
 