﻿using System;
using System.Net.Http;
using CheckInTasiSoft.DAL;
using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Services;
using Christoc.Modules.RegisterModule.Util;
using Christoc.Modules.RegisterModule.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace RegisterModule.Test
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// TEST que consulta el catalogo de razones de la hora fuera del limite
        /// </summary>
        [TestMethod]
        public void GetTimeReasonTest()
        {
            var service = new CheckInService();
            var result = service.GetTimeReason();
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// TEST que consulta el catalogo de razones del cambio de ubicación
        /// </summary>
        [TestMethod]
        public void GetLocationReasonTest()
        {
            var service = new CheckInService();
            var result = service.GetLocationReason();
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// TEST que consulta el catalogo de las extenciones de los archivo
        /// </summary>
        [TestMethod]
        public void GetFileExtensionsTest()
        {
            var service = new CheckInService();
            var result = service.GetFileExtensions();
            Assert.IsFalse(result.IsSuccess);
        }
        /// <summary>
        /// TEST que consulta el obtiene el usuario asignado para los tickets
        /// </summary>
        [TestMethod]
        public void GetUserIDAssignationTest()
        {
            var data = new DataAccess();
            var result = data.GetUserIDAssignation("admin");
            Assert.AreNotEqual(0, result);
        }
        /// <summary>
        /// TEST que valida que la ip sea diferente a al ultima.
        /// </summary>
        [TestMethod]
        public void GetIPNoChangedTest()
        {
            var service = new CheckInService();
            var result = service.GetUserById(1);
            Assert.AreEqual(result.IpAddress.Trim(), "177.242.154.153");
        }
        /// <summary>
        /// TEST que valida que la fecha recuperada sea menor a la fecha presente,
        /// para obtener el ultimo registro del usuario pero que no sea del dia presente
        /// y obtener la ip 
        /// </summary>
        [TestMethod]
        public void GetLastRecordOK_Test()
        {
            var service = new CheckInService();
            var result = service.GetUserById(2);
            var dateNow = DateTime.Now.Date;
            //comparate == 0 -> indica que el valor es igual al esperado
            //comparate == -1 indica que el valor es menor al esperdado
            //comparate == 1 indica que el valor es mayor al esperdado
            var comparate = result.Date.CompareTo(dateNow);
            Assert.AreEqual(-1, comparate);
        }
        /// <summary>
        /// TEST que valida que la fecha recuperada no es menor a la fecha presente,
        /// </summary>
        [TestMethod]
        public void GetLastRecordNOT_Test()
        {
            var service = new CheckInService();
            var result = service.GetUserById(1);
            var dateNow = DateTime.Now.Date;
            //comparate == 0 -> indica que el valor es igual al esperado
            //comparate == -1 indica que el valor es menor al esperdado
            //comparate == 1 indica que el valor es mayor al esperdado
            var comparate = result.Date.CompareTo(dateNow);
            Assert.AreNotEqual(-1, comparate);
        }
        /// <summary>
        /// TEST que valida que la ip no cambio.
        /// </summary>
        [TestMethod]
        public void GetIPChangedTest()
        {
            var service = new CheckInService();
            var result = service.GetUserById(1);
            Assert.AreNotEqual(result.IpAddress.Trim(), "179.282.554.152");
        }
        /// <summary>
        /// TEST que consulta los registros de dia del usuario
        /// </summary>
        [TestMethod]
        public void GetUSerRecordsToday()
        {
            var service = new CheckInService();
            var result = service.GetUSerRecordsToday(1);
            Assert.IsNotNull(result);
        }
        /// <summary>
        /// TEST que realiza el registro en el caso 1
        /// Se registro dentro de la hora y misma ubicación
        /// </summary>
        [TestMethod]
        public void SetEntryTime_TimeOKAndLocationOK_Test()
        {
            var service = new CheckInService();
            var register = new Register();
            var ticket = new Ticket();
            var check = new CheckInVM();
            check.IP = "";
            check.Latitude = null;
            check.Location = null;
            check.TimeOk = true;
            check.LocationOk = true;

            register.Date = DateTime.Now.Date;
            register.Entry = DateTime.Now.TimeOfDay;
            register.IpAddress = "177.242.154.153";
            register.TimeOk = 1;
            register.LocationOk = 1;
            register.IdUser = 1;
            register.Location = string.Format("{0}, {1}", check.Latitude, check.Longitude);
            register.IDEmployee = "";
            register.Assignment = "";

            var result = service.SetEntryTime(register, null, check);
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// TEST que realiza el registro en el caso 2
        /// Se registro fuera de la hora y misma ubicación
        /// </summary>
        [TestMethod]
        public void SetEntryTime_NOTTimeButLocationOK_Test()
        {
            var service = new CheckInService();
            var register = new Register();
            var ticket = new Ticket();
            var check = new CheckInVM();
            check.IP = "";
            check.Latitude = null;
            check.Location = null;
            check.TimeOk = false;
            check.LocationOk = true;
            check.Time = new TimeVM
            {
                IdTimeReason = 1
            };

            //Ticket
            ticket.ModuleID = 10;
            ticket.ChannelID = 0;
            ticket.ReporterID = 1;
            ticket.Email = "TEST";
            ticket.Name = "test@correo.com";
            ticket.Phone = string.Empty;
            ticket.Subject = "";
            ticket.Message = "";
            ticket.StatusID = 1;
            ticket.CreatedOn = DateTime.Now.Date;

            register.Date = DateTime.Now.Date;
            register.Entry = DateTime.Now.TimeOfDay;
            register.IpAddress = "177.242.154.153";
            register.TimeOk = 0;
            register.NameClientTime = "NISSAN";
            register.IdTimeReason = 1;
            register.LocationOk = 1;
            register.IdUser = 1;
            register.Location = string.Format("{0}, {1}", check.Latitude, check.Longitude);
            register.IDEmployee = "";
            register.Assignment = "";
            register.NameUser = "TEST";
            register.LastNameUser = "TEST";
            register.EmailUser = "bryan_silverio@tasisoft.com";

            var result = service.SetEntryTime(register, ticket, check);
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// TEST que realiza el registro en el caso 3
        /// Se registro dentro de la hora y diferente ubicación
        /// </summary>
        [TestMethod]
        public void SetEntryTime_TimeOKButNOTLocation_Test()
        {
            var service = new CheckInService();
            var register = new Register();
            var ticket = new Ticket();
            var check = new CheckInVM();
            check.IP = "177.242.154.153";
            check.Latitude = null;
            check.Location = null;
            check.TimeOk = true;
            check.LocationOk = false;
            check.Location = new LocationVM
            {
                IdLocationReason = 1
            };

            ticket.ModuleID = 10;
            ticket.ChannelID = 0;
            ticket.ReporterID = 1;
            ticket.Email = "TEST";
            ticket.Name = "test@correo.com";
            ticket.Phone = string.Empty;
            ticket.Subject = "";
            ticket.Message = "";
            ticket.StatusID = 1;
            ticket.CreatedOn = DateTime.Now.Date;

            register.Date = DateTime.Now.Date;
            register.Entry = DateTime.Now.TimeOfDay;
            register.IpAddress = "177.242.154.153";
            register.TimeOk = 1;
            register.LocationOk = 0;
            register.NameClientLocation = "NISSAN";
            register.IdLocationReason = 1;
            register.IdUser = 1;
            register.Location = string.Format("{0}, {1}", check.Latitude, check.Longitude);
            register.IDEmployee = "";
            register.Assignment = "";
            register.NameUser = "TEST";
            register.LastNameUser = "TEST";
            register.EmailUser = "bryan_silverio@tasisoft.com";

            var result = service.SetEntryTime(register, ticket, check);
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// TEST que realiza el registro en el caso 4
        /// Se registro fuera de la hora y diferente ubicación
        /// </summary>
        [TestMethod]
        public void SetEntryTime_NOTTimeAndNOTLocation_Test()
        {
            var service = new CheckInService();
            var register = new Register();
            var ticket = new Ticket();
            var check = new CheckInVM();
            check.IP = "177.242.154.153";
            check.Latitude = null;
            check.Location = null;
            check.TimeOk = false;
            check.LocationOk = false;
            check.Location = new LocationVM
            {
                IdLocationReason = 1
            };
            check.Time = new TimeVM
            {
                IdTimeReason = 1
            };

            ticket.ModuleID = 10;
            ticket.ChannelID = 0;
            ticket.ReporterID = 1;
            ticket.Email = "TEST";
            ticket.Name = "test@correo.com";
            ticket.Phone = string.Empty;
            ticket.Subject = "TEST";
            ticket.Message = "TEST";
            ticket.StatusID = 1;
            ticket.CreatedOn = DateTime.Now.Date;

            register.Date = DateTime.Now.Date;
            register.Entry = DateTime.Now.TimeOfDay;
            register.IpAddress = "177.242.154.153";
            register.TimeOk = 0;
            register.IdTimeReason = 1;
            register.NameSupervisorTime = "bernardo";
            register.LocationOk = 0;
            register.OtherReasonLocation = "Trafico";
            register.IdLocationReason = 5;
            register.IdUser = 1;
            register.Location = string.Format("{0}, {1}", check.Latitude, check.Longitude);
            register.IDEmployee = "TEST";
            register.Assignment = "TEST";
            register.NameUser = "TEST";
            register.LastNameUser = "TEST";
            register.EmailUser = "bryan_silverio@tasisoft.com";

            var result = service.SetEntryTime(register, ticket, check);
            Assert.IsTrue(result.IsSuccess);
        }
        /// <summary>
        /// Test que valida que una opcion de las razones del cambio de ubicación
        /// este seleccionada
        /// </summary>
        [TestMethod]
        public void ValidateLocationOptionTest()
        {
            LocationVM locationVM = new LocationVM();
            locationVM.VisitServiceClientOk = true;
            var result = UtilHelper.ValidateLocationOption(locationVM);
            Assert.IsTrue(result);
        }
        /// <summary>
        /// Test que valida que una opción de las razones del la hora fuera del rango
        /// este seleccionada
        /// </summary>
        [TestMethod]
        public void ValidateTimeOptionTest()
        {
            TimeVM timeVM = new TimeVM();
            timeVM.CompensationHoursOk = true;
            var result = UtilHelper.ValidateTimeOption(timeVM);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ValidateFieldTime_Test()
        {
            TimeVM timeVM = new TimeVM();
            timeVM.VisitServiceClientOk = true;
            timeVM.VisitServiceClientTime = "NISSAN";
            var result = UtilHelper.ValidateFieldTime(timeVM);
            Assert.IsTrue(Convert.ToBoolean(result[0]));
        }

        [TestMethod]
        public void ValidateFieldLocation_Test()
        {
            LocationVM locationVM = new LocationVM();
            locationVM.NewAsignationOk = true;
            locationVM.NewAsignation = "NISSAN";
            var result = UtilHelper.ValidateFieldLocation(locationVM);
            Assert.IsTrue(Convert.ToBoolean(result[0]));
        }


        [TestMethod]
        public void ValidateExtension_Test()
        {
            string extencion = "jpg,png,jpeg";
            string file = "image/png";
            var result = UtilHelper.ValidateExtension(extencion, file);
            Assert.IsTrue(result);
        }
    }
}
