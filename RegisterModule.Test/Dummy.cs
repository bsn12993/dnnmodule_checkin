﻿using Christoc.Modules.RegisterModule.ViewModels;
using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegisterModule.Test
{

    public class DummyApiController : DnnApiController
    {
        public List<string> Errores { get; set; }
        public bool EsValido(CheckInVM checkIn)
        {
            Errores = new List<string>();
            Errores.Add("Error");
            return false;
        }
    }
}
