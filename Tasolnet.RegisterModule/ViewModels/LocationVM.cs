﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class LocationVM
    {
        public bool NewAsignationOk { get; set; }
        public string NewAsignation { get; set; }
        public bool VisitServiceClientOk { get; set; }
        public string VisitServiceClientLocation { get; set; }
        public bool HomeOfficeOk { get; set; }
        public string HomeOffice { get; set; }
        public bool BackOfficeOk { get; set; }
        public string BackOffice { get; set; }
        public bool OtherReasonOk { get; set; }
        public string OtherReason { get; set; }
        public int IdLocationReason { get; set; }
    }
}