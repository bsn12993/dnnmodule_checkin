﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    [NotMapped]
    public class UserVM
    {
        public int User_Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Area { get; set; }
        public string EntryTime { get; set; }
        public string DepartureTime { get; set; }
    }
}