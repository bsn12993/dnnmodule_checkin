﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.ViewModels
{
    public class TimeVM
    {
        public bool VisitServiceClientOk { get; set; }
        public string VisitServiceClientTime { get; set; }
        public bool MedicalConsultationOk { get; set; }
        public string MedicalConsultation { get; set; }
        public string MedicalConsultationFileExtension { get; set; }
        public string MedicalConsultationFileName { get; set; }
        public string MedicalConsultationFileSize { get; set; }
        public bool PersonalIncidentOk { get; set; }
        public string PersonalIncident { get; set; }
        public bool CompensationHoursOk { get; set; }
        public string CompensationHours { get; set; }
        public bool OtherReasonOk { get; set; }
        public string OtherReason { get; set; }
        public int IdTimeReason { get; set; }
    }
}