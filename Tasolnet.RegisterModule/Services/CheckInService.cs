﻿using CheckInTasiSoft.DAL;
using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Util;
using Christoc.Modules.RegisterModule.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Services
{
    public class CheckInService
    {
        DataAccess DataAccess = new DataAccess();

        #region Register Entry/Departure
        /// <summary>
        /// Metodo para el registro de hora de entrada
        /// </summary>
        /// <param name="register">objeto con los datos para el registro</param>
        /// <returns></returns>
        public Response SetEntryTime(Register register, Ticket ticket, CheckInVM checkIn)
        {
            Response response = new Response();
            //se valida si cumplio con la ubicación 
            if (!checkIn.LocationOk)
            {
                var subject = (Cat_LocationReason)DataAccess.GetLocationReasonById(checkIn.Location.IdLocationReason).Result;
                ticket.Subject = string.Format("{0} {1}", "Registro desde otra ubicación diferente", subject.NameReason);
                ticket.Message = string.Format("{0}{1}{2}", "<p>Registro desde otra ubicación diferente -", Util.UtilHelper.GetReasonLocation(checkIn.Location), "</p>");
                //Recupera el id del usuario asignado a los tickets, normalmente es para Carlos_Suarez con el id 6
            }
            //se valida si se cumplio con el horario de entrada para almacenar los datos para el generar el ticket
            if (!checkIn.TimeOk)
            {
                var subject = (Cat_TimeReason)DataAccess.GetTimeReasonById(checkIn.Time.IdTimeReason).Result;
                ticket.Message += (ticket.Message.Length == 0) ? "</br>" : "";
                ticket.Message += string.Format("{0}{1}{2}", "<p>Registro fuera del horario limite establecido ", Util.UtilHelper.GetReasonTime(checkIn.Time), "</p>");
                ticket.Subject += string.Format(" {0} {1}", "Registro fuera del horario limite establecido -", subject.NameReason);
            }

            if (!checkIn.LocationOk || !checkIn.TimeOk)
                return DataAccess.InsertEntryTime(register, ticket, checkIn.Time);
            else
                return DataAccess.InsertEntryTime(register, null, null);
        }
        /// <summary>
        ///  Metodo para el registro de hora de salida
        /// </summary>
        /// <param name="register">objeto con los datos para el registro</param>
        /// <returns></returns>
        public Response SetDepartureTime(Register register)
        {
            return DataAccess.InsertDepartureTime(register);
        }
        /// <summary>
        /// Metodo para recuperar registros del usuario en sesión en base a su id
        /// </summary>
        /// <param name="id">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUserById(int id)
        {
            return DataAccess.GetUserById(id);
        }
        /// <summary>
        /// Metodo para recuperar y validar los registros de dia presente del usuario
        /// </summary>
        /// <param name="id">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUSerRecordsToday(int id)
        {
            return DataAccess.GetUSerRecordsToday(id);
        }

        public Response ValidateBirthdayUser(string datebirthday, int iduser)
        {
            var dBirthday = datebirthday.ConvertStringToDateTime();
            return DataAccess.ValidateBirthdayUser(dBirthday, iduser);
        }
        #endregion

        #region Catalogs
        /// <summary>
        /// Metodo que recupera los datos del catalogo de motivos-ubicación
        /// </summary>
        /// <returns></returns>
        public Response GetLocationReason()
        {
            return DataAccess.GetLocationReason();
        }
        /// <summary>
        /// Metodo que recupera los datos del catalogo de motivos-hora
        /// </summary>
        /// <returns></returns>
        public Response GetTimeReason()
        {
            return DataAccess.GetTimeReason();
        }
        #endregion

        #region Tickets
        /// <summary>
        /// Metodo que recupera las extenciones permitidas
        /// </summary>
        /// <returns></returns>
        public Response GetFileExtensions()
        {
            return DataAccess.GetFileExtensions();
        }
        #endregion
    }
}