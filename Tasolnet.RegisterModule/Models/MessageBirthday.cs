﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public static class MessageBirthday
    {
        public static string[] Message = new string[3]
        {
            "A nombre de nuestra gran familia empresarial queremos enviarle un saludo muy especial deseándole un feliz cumpleaños. Que todos sus deseos se hagan realidad.[[br]]",
            "Somos una gran familia empresarial y por eso es motivo de orgullo para nosotros hacerle este pequeño reconocimiento en el día de su cumpleaños. Muchas felicidades.[[br]]",
            "Para nosotros es un gusto contar contigo y a nombre de nuestra compañía, en el día de tu cumpleaños, te deseamos dar nuestras felicitaciones y nuestros más sinceros deseos de que tus metas se hagan realidad.[[br]]"
        };

        public static readonly string MessageBonification =
            @"Como parte de nuestra política de beneficios, el día 15 de este mes recibes una gratificación adicional en tu tarjeta de vales de despensa.";

        public static string GetMessage()
        {
            Random random = new Random();
            int index = random.Next(Message.Length);
            return Message[index];
        }
    }
}