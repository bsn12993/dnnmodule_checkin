﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class Response
    {
        /// <summary>
        /// Almacenara un valor boleano en base a la respuesta
        /// true: si la operación se realizo correctamente
        /// false: si la operación no se realizo correctamente
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Almacenara la descripción del mensaje en base "IsSuccess"
        /// </summary>
        public string Message { get; set; } = string.Empty;
        /// <summary>
        /// Almacenara un objeto de algun resultado de una consulta
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Usado como bandera para determinar si el usuario se registro dentro la hora limite
        /// true: si se registro dentro de la hora limite
        /// false: si se registro fuera de la hora limite
        /// </summary>
        public bool TimeOk { get; set; } = true;
        /// <summary>
        /// Usado como bandera para determinar si el usuario se registro desde el mismo lugar
        /// true: se registro desde la misma ubicación
        /// false: se registro desde otra ubicación
        /// </summary>
        public bool LocationOK { get; set; } = true;
        /// <summary>
        /// Usado como bandera para determinar si el usuario ya registro la hora de entrada antes de registrar la hora de salida
        /// true: ya registro la hora de entrada y puede registrar la salida si asi lo desea
        /// false: aun no ha registrado la hora de entrada, por lo cual no esta permitido registrar la hora de salida aún, se le notificara al usuario
        /// </summary>
        public bool entryOk { get; set; }
    }
}