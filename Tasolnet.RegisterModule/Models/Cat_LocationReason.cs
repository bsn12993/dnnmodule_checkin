﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class Cat_LocationReason
    {
        public int Id { get; set; }
        public string NameReason { get; set; }

        public Cat_LocationReason()
        {
            this.NameReason = string.Empty;
        }
    }
}