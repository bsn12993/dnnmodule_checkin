﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.RegisterModule.Models
{
    public class Ticket
    {
        public int IssueID { get; set; }
        public int ModuleID { get; set; }
        public int ChannelID { get; set; }
        public int ReporterID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int? CategoryID { get; set; }
        public int? PriorityID { get; set; }
        public string Attachments { get; set; }
        public int StatusID { get; set; }
        public int? AgentID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ResolvedOn { get; set; }
        public int? ResolvedBy { get; set; }
        public string AdditionalUsers { get; set; }
        public string Collaborators { get; set; }
        public int? Rating { get; set; }
        public int? AllowSmsNotification { get; set; }

        public Ticket()
        {
            this.Email = string.Empty;
            this.Name = string.Empty;
            this.Phone = string.Empty;
            this.Subject = string.Empty;
            this.Message = string.Empty;
            this.Attachments = string.Empty;
            this.AdditionalUsers = string.Empty;
            this.Collaborators = string.Empty;
        }
    }
}