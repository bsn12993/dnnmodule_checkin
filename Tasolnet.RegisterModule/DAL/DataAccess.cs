﻿using Christoc.Modules.RegisterModule.BD;
using Christoc.Modules.RegisterModule.EntityModels;
using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.Util;
using Christoc.Modules.RegisterModule.ViewModels;
using DotNetNuke.Entities.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CheckInTasiSoft.DAL
{
    public class DataAccess
    {
        SqlParameter[] parameters;
        string sp = string.Empty;
        Register register = null;
        Cat_LocationReason locationReason = null;
        Cat_TimeReason timeReason = null;
        List<Cat_LocationReason> locationReasons = null;
        List<Cat_TimeReason> timeReasons = null;
        Response Response = new Response();
        static string connectionString = string.Empty;
        ConnectionBD connectionBD = new ConnectionBD();

        public DataAccess()
        {
            //Se cambia el ambiente para recuperar la cadena de conexión correspondiente
            //LocalEnvionment -> Local/Desarrollo
            //ServerEnvionment -> Servidor/Producción
            connectionString = connectionBD.GetConnectionLocalString(connectionBD.ServerEnvionment);
        }

        #region Register Entry/Departure
        /// <summary>
        /// Metodo para recuperar los registros de un usuario en base a su id
        /// </summary>
        /// <param name="id">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUserById(int id)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@iduser",id)
            };
            //and date = (select CONVERT(DATE,DATEADD(d,-1,GETDATE())))
            sp = @"SELECT top 1 
	               id_register
                  ,id_user
                  ,entry
                  ,departure
                  ,date
                  ,ip_address
                  ,assignment
                  ,id_employee
                  ,location
	            FROM chk_Register 
	            WHERE id_user=@iduser ORDER BY date DESC";
            try
            {
                using(var connection=new SqlConnection(connectionString))
                {
                    using(var cmd=new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            register = new Register();
                            while (reader.Read())
                            {
                                register.Register_Id = (int)reader["id_register"];
                                register.IdUser = (int)reader["id_user"];
                                register.Entry = (reader["entry"] != DBNull.Value) ? (TimeSpan)reader["entry"] : default(TimeSpan);
                                register.Departure = (reader["departure"] != DBNull.Value) ? (TimeSpan)reader["departure"] : default(TimeSpan);
                                register.Date = (DateTime)reader["date"];
                                register.IpAddress = (reader["ip_address"] != DBNull.Value) ? (string)reader["ip_address"] : string.Empty;
                                register.Assignment = (reader["assignment"] != DBNull.Value) ? (string)reader["assignment"] : string.Empty;
                                register.IDEmployee = (reader["id_employee"] != DBNull.Value) ? (string)reader["id_employee"] : string.Empty;
                                register.Location = (reader["location"] != DBNull.Value) ? (string)reader["location"] : string.Empty;
                            }
                            return register;
                        }
                        else
                        {
                            return register;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                var a = e.Message;
                return null;
            }            
        }
        /// <summary>
        /// Metodo para recuperar y validar los registro del día presente en base al id del  usuario
        /// </summary>
        /// <param name="idusuario">id del usuario en sesión</param>
        /// <returns></returns>
        public Register GetUSerRecordsToday(int idusuario)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@iduser",idusuario)
            };
            sp = @"SELECT *
	                FROM chk_Register 
	                WHERE id_user=@iduser and date = (select CONVERT(DATE,DATEADD(d,0,GETDATE())))";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            register = new Register();
                            while (reader.Read())
                            {
                                register.Register_Id = (int)reader["id_register"];
                                register.IdUser = (int)reader["id_user"];
                                register.Entry = (TimeSpan)reader["entry"];
                                register.Departure = (reader["departure"] != DBNull.Value) ? (TimeSpan)reader["departure"] : new TimeSpan(0, 0, 0);
                                register.Date = (DateTime)reader["date"];
                                register.IpAddress = (reader["ip_address"] != DBNull.Value) ? (string)reader["ip_address"] : string.Empty;
                                register.Assignment = (reader["assignment"] != DBNull.Value) ? (string)reader["assignment"] : string.Empty;
                                register.IDEmployee = (reader["id_employee"] != DBNull.Value) ? (string)reader["id_employee"] : string.Empty;
                                register.Location = (reader["location"] != DBNull.Value) ? (string)reader["location"] : string.Empty;
                            }
                            return register;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                return null;
            }
        }
        /// <summary>
        /// Metodo para registrar la hora de entrada del usuario
        /// </summary>
        /// <param name="register">objeto con los datos para el registro de la hora entrada</param>
        /// <returns></returns>
        public Response InsertEntryTime(Register register, Ticket ticket, TimeVM timeVM)
        {
            object idTimeReason = null;
            object idLocationReason = null;
            SqlTransaction sqlTransaction = null;
            object Attachments = null;
            object Assignment = null;
            SqlParameter[] sqlParameterTicket = null;

            if (register.IdTimeReason == 0) idTimeReason = DBNull.Value;
            else idTimeReason = register.IdTimeReason;

            if (register.IdLocationReason == 0) idLocationReason = DBNull.Value;
            else idLocationReason = register.IdLocationReason;

            if (string.IsNullOrEmpty(register.Assignment)) Assignment = DBNull.Value;
            else Assignment = register.Assignment;

             parameters = new SqlParameter[]
            {
                new SqlParameter("@entry",register.Entry),
                new SqlParameter("@iduser",register.IdUser),
                new SqlParameter("@date",DateTime.Now.Date),
                new SqlParameter("@departure",new TimeSpan(0,0,0)),
                new SqlParameter("@ipaddress",register.IpAddress),
                new SqlParameter("@assignment",Assignment),
                new SqlParameter("@location",register.Location),
                new SqlParameter("@idemployee",register.IDEmployee),
                new SqlParameter("@timeOk",register.TimeOk),
                new SqlParameter("@locationOk",register.LocationOk),
                new SqlParameter("@nameClientTime",register.NameClientTime),
                new SqlParameter("@nameSupervisorTime",register.NameSupervisorTime),
                new SqlParameter("@personalIncidentTime",register.PersonalIncidentTime),
                new SqlParameter("@otherReasonTime",register.OtherReasonTime),
                new SqlParameter("@medicalVoucherfile",register.MedicalVoucherFile),
                new SqlParameter("@idTimeReason",idTimeReason),
                new SqlParameter("@nameClientLocation",register.NameClientLocation),
                new SqlParameter("@nameSupervisorLocation",register.NameSupervisorLocation),
                new SqlParameter("@otherReasonLocation",register.OtherReasonLocation),
                new SqlParameter("@idLocationReason",idLocationReason)
        };
            sp = @"INSERT INTO chk_Register
                       (id_user
                       ,entry
                       ,date
                       ,ip_address
                       ,assignment
                       ,id_employee
                       ,location
                       ,timeOk
                       ,locationOk
                       ,name_client_time
                       ,name_supervisor_time
                       ,name_client_location
                       ,name_supervisor_location
                       ,incident_personal_time
                       ,medical_voucher_file
                       ,other_reason_time
                       ,other_reason_location
                       ,id_location_reason
                       ,id_time_reason)
                 VALUES
                       (@iduser
                       ,@entry
                       ,@date
                       ,@ipaddress
                       ,@assignment
                       ,@idemployee
                       ,@location
                       ,@timeOk
                       ,@locationOk
                       ,@nameClientTime
                       ,@nameSupervisorTime
                       ,@nameClientLocation
                       ,@nameSupervisorLocation
                       ,@personalIncidentTime
                       ,@medicalVoucherfile
                       ,@otherReasonTime
                       ,@otherReasonLocation
                       ,@idLocationReason
                       ,@idTimeReason)";
         
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        sqlTransaction = connection.BeginTransaction();
                        cmd.Transaction = sqlTransaction;
                        //Sin Ticket generado
                        var reader = cmd.ExecuteNonQuery();
                        if (reader > 0)
                        {
                            if(ticket!=null && timeVM != null)
                            {
                                var emailSender = EmailHelper.SendEmail(ticket.IssueID, ticket.Message, ticket.Subject, register.FullNameUser, register.EmailUser, timeVM.MedicalConsultationOk, timeVM.MedicalConsultationFileName);
                                if (emailSender.IsSuccess)
                                {
                                    sqlTransaction.Commit();
                                    Response.IsSuccess = true;
                                    Response.Message = "Se ha registrado la entrada";
                                }
                                else
                                    throw new Exception("No se pudo registrar la entrada y el envio de correo");
                            }
                            else
                            {
                                sqlTransaction.Commit();
                                Response.IsSuccess = true;
                                Response.Message = "Se ha registrado la entrada";
                            }
                        }
                        else
                            throw new Exception("No se pudo registrar la entrada");
                    }
                }
            }
            catch (Exception e)
            {
                
                Response.IsSuccess = false;
                Response.Message = string.Format("{0} {1}", "No se pudo registrar la entrada: ", e.Message);
            }
            return Response;
        }
        /// <summary>
        /// Metodo para registrar la hora de salida del usuario
        /// </summary>
        /// <param name="register">objeto con los datos para el registro de la hora salida</param>
        /// <returns></returns>
        public Response InsertDepartureTime(Register register)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@departure",register.Departure),
                new SqlParameter("@iduser",register.IdUser)
            };
            sp = @"UPDATE chk_Register
	                 SET departure = @departure
	                 WHERE id_user=@iduser and date=(select CONVERT(DATE,DATEADD(d,0,GETDATE())))";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteNonQuery();
                        if (reader > 0)
                        {
                            Response.IsSuccess = true;
                            Response.Message = "Se ha registrado la salida";
                        }
                        else
                        {
                            Response.IsSuccess = false;
                            Response.Message = $"No se pudo registrar la salida - departure:{register.Departure} user: {register.IdUser}";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
            }
            return Response;
        }
        /// <summary>
        /// Metodo que actualiza el campo ticket del registro de entrada
        /// </summary>
        /// <param name="ticket">id del ticket generado</param>
        /// <param name="idusuario">id del usuario en sesion</param>
        /// <returns></returns>
        public Response UpdateNumTicket(int ticket,int idusuario)
        {
            sp = @"update r
                    set
                     r.ticket=@ticket
                    from chk_Register r
                    WHERE id_user=@iduser and date = (select CONVERT(DATE,DATEADD(d,0,GETDATE())))";
            parameters = new SqlParameter[]
            {
                new SqlParameter("@iduser",idusuario),
                new SqlParameter("@ticket",ticket)
            };
            try
            {
                using(var connection=new SqlConnection(connectionString))
                {
                    using(var cmd=new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var update = cmd.ExecuteNonQuery();
                        if (update > 0)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se ha registrado la entrada",
                                Result = null
                            };
                        }
                        else
                            throw new Exception("No se pudo registrar la entrada");
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = true,
                    Message = e.Message,
                    Result = null
                };
            }
        }
        #endregion

        #region Catalogs
        /// <summary>
        /// Metodo para recuperar los datos del catalogo de motivos de cambio de ubicación
        /// </summary>
        /// <returns></returns>
        public Response GetLocationReason()
        {
            locationReasons = new List<Cat_LocationReason>();
            sp = @"select * from chk_LocationReason where status=1";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {            
                            while (reader.Read())
                            {
                                locationReason = new Cat_LocationReason();
                                locationReason.Id = (int)reader["id_location_reason"];
                                locationReason.NameReason = (string)reader["descripcion"];
                                locationReasons.Add(locationReason);
                            }
                            Response.IsSuccess = true;
                            Response.Message = "Se encontrarón datos";
                            Response.Result = locationReasons;
                            return Response;
                        }
                        else
                        {
                            Response.IsSuccess = true;
                            Response.Message = "No se encontrarón datos";
                            Response.Result = locationReasons;
                            return Response;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                Response.Result = null;
                return Response;
            }
        }
        /// <summary>
        /// Metodo para recuperar un registro del catalogo por su id
        /// </summary>
        /// <param name="id">identificador del registro</param>
        /// <returns></returns>
        public Response GetLocationReasonById(int id)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@id",id)
            };
            sp = @"select * from chk_LocationReason where status=1 and id_location_reason=@id";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            locationReason = new Cat_LocationReason();
                            while (reader.Read())
                            {                           
                                locationReason.Id = (int)reader["id_location_reason"];
                                locationReason.NameReason = (string)reader["descripcion"];
                            }
                            Response.IsSuccess = true;
                            Response.Message = "Se encontrarón datos";
                            Response.Result = locationReason;
                            return Response;
                        }
                        else
                        {
                            Response.IsSuccess = true;
                            Response.Message = "No se encontrarón datos";
                            Response.Result = null;
                            return Response;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                Response.Result = null;
                return Response;
            }
        }
        /// <summary>
        /// Metodo para recuperar los datos del catalogo de motivos de hora fuera del limite
        /// </summary>
        /// <returns></returns>
        public Response GetTimeReason()
        {
            timeReasons = new List<Cat_TimeReason>();
            sp = @"select * from chk_TimeReason where status=1";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {   
                            while (reader.Read())
                            {
                                timeReason = new Cat_TimeReason();
                                timeReason.Id = (int)reader["id_time_reason"];
                                timeReason.NameReason = (string)reader["descripcion"];
                                timeReasons.Add(timeReason);
                            }
                            Response.IsSuccess = true;
                            Response.Message = "Se encontrarón datos";
                            Response.Result = timeReasons;
                            return Response;
                        }
                        else
                        {
                            Response.IsSuccess = true;
                            Response.Message = "No se encontrarón datos";
                            Response.Result = timeReasons;
                            return Response;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                Response.Result = null;
                return Response;
            }
        }
        /// <summary>
        /// Metodo para recuperar un registro del catalogo por su id
        /// </summary>
        /// <param name="id">identificador del registro</param>
        /// <returns></returns>
        public Response GetTimeReasonById(int id)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@id",id)
            };
            sp = @"select * from chk_TimeReason where status=1 and id_time_reason=@id";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            timeReason = new Cat_TimeReason();
                            while (reader.Read())
                            {                
                                timeReason.Id = (int)reader["id_time_reason"];
                                timeReason.NameReason = (string)reader["descripcion"];
                            }
                            Response.IsSuccess = true;
                            Response.Message = "Se encontrarón datos";
                            Response.Result = timeReason;
                            return Response;
                        }
                        else
                        {
                            Response.IsSuccess = true;
                            Response.Message = "No se encontrarón datos";
                            Response.Result = null;
                            return Response;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                Response.Result = null;
                return Response;
            }
        }
        #endregion

        #region Tickets

        public Response GetTicketUser()
        {
            int ticket = 0;
            sp = @"SELECT MAX(IssueID) ticket
                FROM LiveHelpdesk_Issue";
            try
            {
                using(var connection=new SqlConnection(connectionString))
                {
                    using(var cmd=new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ticket = (int)reader["ticket"];
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se encontro ticket",
                                Result = ticket
                            };
                        }
                        else
                            throw new Exception("no se recupero el ticket");
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                    Result = ticket
                };
            }
        }

        /// <summary>
        /// Metodo que consulta las extenciones permitidas en el help desk
        /// </summary>
        /// <returns></returns>
        public Response GetFileExtensions()
        {
            string[] extensions = { "jpg", "jpeg", "gif", "png", "pdf" };
            FileExtension fileExtension = new FileExtension();
            fileExtension.AllowedAttachmentFileExtensions = string.Join(",", extensions);
            Response.IsSuccess = true;
            Response.Result = fileExtension;
            return Response;
        }
        #endregion

        /// <summary>
        /// Metodo que recupera el id del asignado a los tickets en base a su nombre de usuario
        /// Por lo normal es Carlos Suarez, con el id 6
        /// </summary>
        /// <param name="Username">nombre del usuario</param>
        /// <returns></returns>
        public int GetUserIDAssignation(string Username)
        {
            sp = @"select UserID from Users where Username=@Username";
            int UserID = 0;
            parameters = new SqlParameter[]
            {
                new SqlParameter("@Username",Username)
            };

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                UserID = (int)reader["UserID"];
                            }
                            return UserID;
                        }
                        else
                            return default(int);
                    }
                }
            }
            catch (Exception e)
            {
                return default(int);
            }
        }

        /// <summary>
        /// Metodo que recupera y valida si el usuario cumplio años el dia presente 
        /// o dias anteriores (en caso de haber sido en fin de semana), para mostrar el mensaje de 
        /// Felicitaciones
        /// </summary>
        /// <param name="datebirthday">fecha de cumpleaños del usuario recuperado del DNN</param>
        /// <param name="iduser">identificador del usuario</param>
        /// <returns></returns>
        public Response ValidateBirthdayUser(DateTime datebirthday, int iduser)
        {
            sp = @"SELECT 
                    id_register
                    ,id_user
                    ,MONTH(date) month
                    ,DAY(date) day
                    FROM chk_Register 
                    WHERE 
                    id_user=@iduser and 
                    MONTH(date)=MONTH(@datebirthday)
                    and DAY(@datebirthday)<=DAY(date)
                    ORDER BY date DESC";

            parameters = new SqlParameter[]
            {
                new SqlParameter("@iduser",iduser),
                new SqlParameter("@datebirthday",datebirthday)
            };
            try
            {
                using(var connection=new SqlConnection(connectionString))
                {
                    using(var cmd=new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        //Si existe registro significa que si registro el dia de su cumpleaños (ayer)
                        //y no es necesario mostrar el mensaje el dia presente
                        if (reader.HasRows)
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "Si registro el día de su cumpleaños ayer",
                                Result = ""
                            };
                        }
                        //Si no existe registro significa que no registro el dia de su cumpleaños (ayer)
                        //y entonces se tendria que mostrar el mensaje el dia presente
                        else
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "No registro el día de su cumpleaños ayer",
                                Result = ""
                            };
                        }
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                    Result = null
                };
            }
        }
    }
}
 