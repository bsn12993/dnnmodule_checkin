﻿using Christoc.Modules.RegisterModule.Models;
using Christoc.Modules.RegisterModule.ViewModels;
using DotNetNuke.Entities.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace Christoc.Modules.RegisterModule.Util
{
    public static class EmailHelper
    {
        public static Email email = new Email();

        public static Response SendEmail(int ticket, string msg, string title, string user,string userEmail, bool hasFile, string file)
        {
            MailMessage message = new MailMessage();
            MailMessage messageResponsable = new MailMessage();
            //destinatario
            message.To.Add(userEmail);
 
            messageResponsable.To.Add(email.DestinationEmail);

            message.Subject = $"Issue Created in Soporte Técnico - {user}";
            message.SubjectEncoding = Encoding.UTF8;

            messageResponsable.Subject = $"Issue - {user}";
            messageResponsable.SubjectEncoding = Encoding.UTF8;

            email.Body = email.Body.Replace("[[ticket]]", ticket.ToString());
            email.Body = email.Body.Replace("[[user]]", user);
            email.Body = email.Body.Replace("[[title]]", title);
            email.Body = email.Body.Replace("[[issue]]", msg);
            email.Body = email.Body.Replace("[[date]]", DateTime.Now.Date.ToShortDateString());

            email.BodyResponsable = email.BodyResponsable.Replace("[[ticket]]", ticket.ToString());
            email.BodyResponsable = email.BodyResponsable.Replace("[[user]]", user);
            email.BodyResponsable = email.BodyResponsable.Replace("[[title]]", title);
            email.BodyResponsable = email.BodyResponsable.Replace("[[issue]]", msg);
            email.BodyResponsable = email.BodyResponsable.Replace("[[date]]", DateTime.Now.Date.ToShortDateString());

            message.Body = email.Body;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;

            messageResponsable.Body = email.BodyResponsable;
            messageResponsable.BodyEncoding = Encoding.UTF8;
            messageResponsable.IsBodyHtml = true;

            if (hasFile)
            {
                Attachment attachmentResponsable = new Attachment(file);
                attachmentResponsable.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                messageResponsable.Attachments.Add(attachmentResponsable);

                Attachment attachment = new Attachment(file);
                attachment.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                message.Attachments.Add(attachment);
            }

            message.From = new MailAddress(email.SenderEmail);
            messageResponsable.From = new MailAddress(email.SenderEmail);

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Credentials = new NetworkCredential(email.SenderEmail, email.Password);

            smtpClient.Port = email.Port;
            smtpClient.EnableSsl = email.SSL;
            smtpClient.Host = email.Host;
            try
            {
                smtpClient.Send(message);
                smtpClient.Send(messageResponsable);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Correo enviado correctamente"
                };
            }
            catch(System.Net.Mail.SmtpException e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }         
        }
    }
}